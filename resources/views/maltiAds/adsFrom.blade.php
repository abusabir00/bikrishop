@extends('layouts.app')
@section('content')

  <div class="container">
    <div class="row">
      <div class="col-md-6">

        <form action="{{ url('store-malti-ads') }}" method="post" enctype="multipart/form-data"> 
          {{ csrf_field() }}
          <br><br><div class="card">          		
            <div class="card-header bg-dark">       
             <h3 class="text-warning text-center" style="color: #009877">Advertizeing form</h3>
           </div>

           <div class="form-group{{ $errors->has('campaignname') ? ' has-error' : '' }}">
            
              <label for="campaignname" class="control-label" style="color: #009877">Campaign Name:</label>
                <input id="campaignname" type="text" class="form-control" name="campaignname" required autocomplete="off">

              @if ($errors->has('campaignname'))
              <span class="help-block">
                <strong>{{ $errors->first('campaignname') }}</strong>
              </span>
              @endif
            
          </div>

          <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
            
              <label for="link" class="control-label" style="color: #009877">Redirect URL:</label>
                <input id="link" type="text" class="form-control" name="link" required autocomplete="off">

              @if ($errors->has('link'))
              <span class="help-block">
                <strong>{{ $errors->first('link') }}</strong>
              </span>
              @endif
          
          </div>

          <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            
              <label for="image" class="control-label" style="color: #009877">Ads image or GIf:</label>
                <input id="image" type="file" class="form-control" name="image" required autocomplete="off">

              @if ($errors->has('image'))
              <span class="help-block">
                <strong>{{ $errors->first('image') }}</strong>
              </span>
              @endif
            
          </div>

          <div class="form-group{{ $errors->has('startdate') ? ' has-error' : '' }}">
            
              <label for="startdate" class="control-label" style="color: #009877">Ads Starting date:</label>
                <input id="startdate" type="date" class="form-control" name="startdate" required autocomplete="off">

              @if ($errors->has('startdate'))
              <span class="help-block">
                <strong>{{ $errors->first('startdate') }}</strong>
              </span>
              @endif
         
          </div>


          <div class="form-group{{ $errors->has('package') ? ' has-error' : '' }}">
            
              <label for="package" class="control-label" style="color: #009877">Ads image or GIf:</label>
              <select class="form-control" name="package" required>
               <option value="">Select One</option>
               <option value="5 Days">5 Days</option>
               <option value="7 Days">7 Days</option>
               <option value="15 Days">15 Days</option>
               <option value="1 Month">1 Month</option>
               <option value="3 Month">3 Month</option>
             </select>

             @if ($errors->has('package'))
             <span class="help-block">
              <strong>{{ $errors->first('package') }}</strong>
            </span>
            @endif
   
        </div>

        <div class="form-group{{ $errors->has('transectionId') ? ' has-error' : '' }}">
          
            <label for="transectionId" class="control-label" style="color: #009877">Bkash trnx id :</label>
              <input id="transectionId" type="text" class="form-control" name="transectionId" required  autocomplete="off">

            @if ($errors->has('transectionId'))
            <span class="help-block">
              <strong>{{ $errors->first('transectionId') }}</strong>
            </span>
            @endif
          
        </div>


        <div class="form-group{{ $errors->has('mobileNum') ? ' has-error' : '' }}">
          
            <label for="mobileNum" class="control-label" style="color: #009877">Mobile Num :</label>
              <input id="mobileNum" type="text" class="form-control" name="mobileNum" required  autocomplete="off">

            @if ($errors->has('mobileNum'))
            <span class="help-block">
              <strong>{{ $errors->first('mobileNum') }}</strong>
            </span>
            @endif
          
        </div>
       <br> <button class="btn btn-warning" type="submit">Submit</button><br>
      </div>

    </form>
  </div>

  <div class="col-md-6">

    <br><br>
    <div class="card">

       <div class="card-header bg-dark">       
        <h3 class="text-warning text-center" style="color: #009877">Advertizeing Packages</h3>
      </div><br><br>


  <div class="container">
  <ul class="nav nav-pills">
    <li class="active"><a class="btn btn-warning" data-toggle="pill" href="#home">CUSTOM AD</a></li>
    <li><a class="btn btn-warning" data-toggle="pill" href="#menu1">7 DAYS</a></li>
    <li><a class="btn btn-warning" data-toggle="pill" href="#menu2">15 DAYS</a></li>
    <li><a class="btn btn-warning" data-toggle="pill" href="#menu3">1 MONTH</a></li>
    <li><a class="btn btn-warning" data-toggle="pill" href="#menu4">3  MONTH</a></li>
  </ul>
  
  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
            <div class="container">

        <h4 class="text-success"><strong class="text-danger">50</strong> Taka Per day</h4><br>
        <form action="#" class="form-inline">

          <input style="border: 4px solid #00800057; width: 30%" type="text" id="second" class="form-control" placeholder="How Many days"><br>

        </form>

        <h4 id="result" class="text-danger"></h4>

      </div>
    </div><br>
    <div id="menu1" class="tab-pane fade">
      <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
  <div class="card-header">7 DAYS</div>
  <div class="card-body">
    <h4 class="text-dark"><strong class="text-danger">1000</strong> Taka </h4><br>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
    </div>

    <div id="menu2" class="tab-pane fade">
     <div class="card text-white bg-warning mb-3" style="max-width: 18rem;">
  <div class="card-header">15 DAYS</div>
  <div class="card-body">
    <h5 class="card-title">Warning card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
    </div>
    <div id="menu3" class="tab-pane fade">
      <div class="card text-white bg-info mb-3" style="max-width: 18rem;">
  <div class="card-header">1 MONTH</div>
  <div class="card-body">
    <h5 class="card-title">Info card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
    </div>

    <div id="menu4" class="tab-pane fade">
      <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
  <div class="card-header">3 MONTH</div>
  <div class="card-body">
    <h5 class="card-title">Dark card title</h5>
    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
  </div>
</div>
    </div>

  </div>
</div>




    </div>

  </div>
</div>
</div>

<script type="text/javascript">

  var second = document.getElementById('second');
  var result = document.getElementById('result');


  second.addEventListener("input", sum);

  function sum() {

    var two = parseFloat(second.value) || 0;

    var add = 50*two;

    result.innerHTML = "Campaing Budget : " + add +" BDT";

  }

</script>
@endsection