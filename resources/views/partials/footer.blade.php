<?php
$cat = DB::table('categories')->where('status', 1)->groupBy('name')->get()->toArray();

$category = array(
        'categories' => array(),
        'parent_cats' => array()
);
//build the array lists with data from the category table
foreach ($cat as $row) {
    //creates entry into categories array with current category id ie. $categories['categories'][1]
    $category['categories'][$row->id] = $row;
    //creates entry into parent_cats array. parent_cats array contains a list of all categories with children
    $category['parent_cats'][$row->parent_id][] = $row->id;
}
?>

<style type="text/css">
    
    .footer{
    background: #ffffff;
    border-top: 4px solid #009877;
    color: #0c00c3;
  
  .links{
    ul {list-style-type: none;}
    li a{
      color: white;
      transition: color .2s;
      &:hover{
        text-decoration:none;
        color:#337ab7;
        }
    }
  }  
  .about-company{
    i{font-size: 25px;}
    a{
      color:white;
      transition: color .2s;
      &:hover{color:#4180CB}
    }
  } 
  .location{
    i{font-size: 18px;}
  }
  .copyright p{border-top:1px solid rgba(255,255,255,.1);} 
}

@media only screen and (max-width: 600px) {
    
    p{
      text-align:center;
    }

    h5{
      text-align:center;
      margin-top:10px;
    }

    h4{
      text-align:center;
      margin-top:10px;
    }

   .footer_image img{

      margin-left: 80px;
    }

    svg{
      margin-left: -65px;
    }

}

</style>
<div class="mt-5 pt-5 pb-5 footer">
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-xs-12 about-company">
      <h5 style="color: black">We're At Google Play Store</h5>
      <p style="font-size: 15px;color: black">Click Below To Download Our Apps</p>
      <div class="footer_image">
      <img src="https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png" style="width: 170px;">
      </div>
    </div>
    <div class="col-lg-3 col-xs-12 links">
      <h5 class="mt-lg-0 mt-sm-3" style="color: black; font-size: 18px;">Our Terms</h5>
       <?php
        $pages = \App\CustomPage::get();
       ?>
       
        <ul class="m-0 p-0">
      @foreach($pages as $item)
       @if($item->status == 1)
          <li>- <a href="{{ url('page/'.$item->slug) }}">{{ ucfirst($item->title) }}</a></li>
          
          @endif
      @endforeach
      <li>- <a href="https://www.youtube.com/watch?v=uqKo5VR6Gwg&feature=youtu.be" class="btn-primary"> YouTube</a></li>
        <li>- <a href="https://www.facebook.com/bikrishopbd" class="btn-primary"> Social</a></li>
        <li>- <a href="{{route('contact-us')}}" class="btn-primary">Mail Author</a></li>
        
        </ul>
        
        

    </div>
    <div class="col-lg-3 col-xs-12 links">
      <h5 class="mt-lg-0 mt-sm-3" style="color: black;font-size: 18px;">More About Us</h5>
        <?php
        $pages = \App\CustomPage::get();
       ?>
       @foreach($pages as $item)
       @if($item->status == 0)
        <ul class="m-0 p-0">
          <li>- <a href="{{ url('page/'.$item->slug) }}">{{ ucfirst($item->title) }}</a></li>
          
        </ul>
        @endif
        @endforeach
    </div>
    <div class="col-lg-3 col-xs-12 location">
      <h4 class="mt-lg-0 mt-sm-4" style="color: black;">Location</h4>
      <p>22th Avenue, Dhaka, Bangladesh</p>
      <p class="mb-0"><i class="fa fa-phone mr-3"></i> (+880) 1817 121233</p>
      <p><i class="fa fa-envelope-o mr-3"></i> support@bikrishop.com</p>
    </div>
  </div>

   
  <div class="row mt-5">
    <div class="col copyright" style="border-top: 1px solid #8080803d"><br>
      <p class="text-center">© 2020. All Rights Reserved | Developed by <a href="https://roxbd.com/">ROXBD.</a></a></p>
    </div>
  </div>
</div>
</div>
</div>

<!-- LOADING  -->
<div style=" top: 0px; bottom: 0px; left: 0px; position: fixed; width: 100%; z-index: 999999; display: none; background: rgba(0,0,0,0.5);" id="loading">
    <div style="margin: 20% 45%; text-align: center;">
        <img src="{!! asset('assets/images/loader1.gif') !!}" alt=""  class="loading"><br />
        <span style="color: mintcream;"> Processing...</span>
    </div>
</div>
<!-- Sweet-Alert  -->
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') }}"></script>

<script src="{{ asset('assets/js/vendors.min.js') }}"></script>
<script src="{{ asset('assets/js/script.js') }}"></script>
<!-- dataTables JS  -->
<script src="{{ asset('assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/plugins/datatables/dataTables.bootstrap.js') }}"></script>

<!-- Toastr js -->
<script src="{{ asset('admin_assets/plugins/toastr/toastr.min.js') }}"></script>

<script src="{{ asset('assets/js/common.js') }}"></script>

@yield('js')

@if( !Auth::guest())
    <script>
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": true,
            "positionClass": "toast-bottom-left",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
        // check login status
        setInterval(function() {

            $.get('{{ url('login_status') }}', function(data){

            });
            //
        }, 99000);
    </script>

    @if($setting->live_chat!=0)
        @include('chat.test')
    @endif
@endif


@if($setting->translate == 1)
<script type="text/javascript">
    function googleTranslateElementInit() {
        new google.translate.TranslateElement({
            pageLanguage: 'en',
            //includedLanguages: 'et',
            layout: google.translate.TranslateElement.InlineLayout.SIMPLE,
            autoDisplay: false
        }, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>

@endif
{!! @$setting->footer_script !!}
</body>
</html>