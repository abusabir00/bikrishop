@extends('layouts.app')
@section('content')
    <style>
        .price-box img {
            height: 20px !important;
        }

        .irs-single {
            margin-left: 9px;
        }

        .btn-auths {
            padding: 2px 7px !important;
        }

        .ads-details .btn {
            white-space: normal;
        }

        input[type='text'].form-control {
            padding: 5px 5px
        }

        .form-control {
            border: 1px solid #c0c0c0;
            /*height: 38px !important;*/
        }

        #cf_field .col-md-2 {
            padding: 0 1px !important;
        }

        #cf_field .col-md-2:first-child {
            padding-left: 5px !important;
        }

        #search_in_search_page{

            /*border:1px solid red;*/

            margin: 0 auto;
            margin-top: 10px;
            margin-bottom: 10px;
            max-width: 500px;

        }
        .search-row{
            margin-top: 0 !important;
        }

        .item_indiv{

            cursor: pointer;

        }
        @media (max-width: 500px) {
        #side_banner{

           display: none;
        }

        }


        @media (max-width: 500px) {

            #side_banner{
                display: none;
            }
        }

        @media (max-width: 767px){

            .listings-item{

                width:250px !important;
                height:250px !important;
                margin: 0 auto;

            }

            .panel-group-mobile {display:block !important;}
            .panel-group {display:none;}

         }




        @media (max-width: 450px){
         .item_indiv{
                margin-bottom: 70px;
            }

        }

        .error{
            color:red;
        }


            /*.mySlides {display:none;}*/

    </style>
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
    <!-- ION Slider -->
    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.skinFlat.css') }}" rel="stylesheet"
          type="text/css"/>

    <?php
    $setting = DB::table('setting')->first();
    $req_region = $req_keyword = $req_price_sort = $low_price = $high_price = $online = '';

    if (isset($_REQUEST['online']))
        $online = $_REQUEST['online'];
    if (isset($_REQUEST['region']))
        $req_region = $_REQUEST['region'];
    if (isset($_REQUEST['keyword']))
        $req_keyword = $_REQUEST['keyword'];
    if (isset($_REQUEST['price_sort']))
        $req_price_sort = $_REQUEST['price_sort'];
    if (isset($_REQUEST['price_range'])) {
        $price_exp = explode(';', $_REQUEST['price_range']);
        $low_price = $price_exp[0];
        $high_price = $price_exp[1];
    } else {
        $low_price = 1;
        $high_price = 10000000;
    }
    $image = 1;
    if (isset($_REQUEST['image']))
        $image = $_REQUEST['image'];
    ?>
    <?php




    function buildCategory($parent, $category)
    {
        //print_r($category['parent_cats'][$parent]);
        $html = $bold = "";
        if (isset($category['parent_cats'][$parent])) {
            $html .= "";
            foreach ($category['parent_cats'][$parent] as $cat_id) {
                if (!isset($category['parent_cats'][$cat_id])) {
                    $html .= "<option value='" . $cat_id . "'>" . ucfirst($category['categories'][$cat_id]['name']) . "</option>";
                }
                if (isset($category['parent_cats'][$cat_id])) {
                    if ($category['categories'][$cat_id]['parent_id'] == 0) {
                        $html .= "<optgroup label='" . ucfirst($category['categories'][$cat_id]['name']) . "'>";
                    } else {
                        $html .= "<option disabled value='" . $cat_id . "'> " . ucfirst($category['categories'][$cat_id]['name']) . "</option>";
                    }
                    //$html .= "<optgroup label='" . ucfirst($category['categories'][$cat_id]['name']) . "'>";
                    $html .= buildCategory($cat_id, $category);
                    $html .= "</optgroup>";
                }
            }
            $html .= "";
        }
        $html .= '';
        return $html;
    }
    ?>
    <section id="main" class="clearfix home-default">
        <div class="container">
                        




    <div style="margin-top:5px" class="main-container">
        <div class="container">
            <div class="col-md-12_000000">
               <div class="adengine">
                <?php $current_date = Date("Y-m-d H:i:s"); ?>
                @foreach($multiAdsShow as $value)
                @if($current_date <= $value->enddate)
                <a href="{{ $value->link }}" target="_blank"><img class="mySlides" src="../public/upload/ads/{{ $value->image }}" style="width:100%; height: 150px;"></a>
                @endif
                @endforeach
                </div>
            </div>
        </div>
    </div><br/><br/>





            <div class="category-info">
                <div class="row">
                    <!-- accordion-->
                    <div class="col-sm-4 col-md-3">
                        <div class="accordion">
                            <!-- panel group for desktop-->
                                <div class="panel-group" id="accordion">
                                <!-- panel -->
                                <div class="panel-default panel-faq">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordion-one">
                                            <h4 class="panel-title" style="color:#009877;">All Categories<span
                                                        class="pull-right"><i class="fa fa-minus" id="all_cat_item"></i></span></h4>
                                        </a>
                                    </div><!-- panel-heading -->

                                    <div id="accordion-one" class="panel-collapse collapse in">
                                        <!-- panel-body -->
                                        <div class="panel-body">
                                            <ul>
                                                @foreach(\App\Category::select('name','slug','icon', 'image')->where(['parent_id'=>0, 'status' => 1 ])->get() as $cat )
                                                    <li @if(str_replace('-', ' ', @$_REQUEST['main_category'] ) == strtolower($cat->name)) class="active" @endif >
                                                        <a style="color: #009877"
                                                           href="{{url('search/query?main_category='.urlencode($cat->slug))}}">{{ ucwords($cat->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->
                                <!-- panel -->
                                @if( $setting->search_ads == 1 && $setting->search_ads_p == 'r' )
                                    <div class="inner-box">
                                        <!-- ads box -->
                                        {!! $setting->search_adsense !!}
                                    </div>
                                @endif
                                <div class="panel-default panel-faq">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordion-two">
                                            <h4 class="panel-title" style="color: #009877">Filters<span
                                                        class="pull-right"><i class="fa fa-minus" id="filter-item"></i></span></h4>
                                        </a>
                                    </div><!-- panel-heading -->
                                    <div id="accordion-two" class="panel-collapse in collapse">
                                        <!-- panel-body -->
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="price_range" class=" control-label"><b
                                                            style="color: #009877">Price ({{$setting->currency}}
                                                        )</b><span
                                                            class="font-normal text-muted f-s-12 clearfix"></span></label>
                                                <div class="">
                                                    <input type="text" id="price_range" form="search_form"
                                                           name="price_range">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="online" class="" style="color: #009877">User Online /
                                                    Offline</label>
                                                <select name="online" id="" class="form-control" form="search_form">
                                                    <option @if( $online == '') selected @endif  value="">All</option>
                                                    <option @if( $online == 1) selected @endif  value="1">Online
                                                    </option>
                                                    <option @if( $online == 2 ) selected @endif  value="2">Offline
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="img" style="color: #009877">
                                                    <input type="radio" name="image" value="1" id="img"
                                                           {{ ($image==1)? 'checked':'' }} form="search_form">
                                                    Image
                                                </label>
                                                <label for="no-img" class="m-l-5" style="color: #009877">
                                                    <input type="radio" name="image" value="0" id="no-img"
                                                           {{ ($image==0)? 'checked':'' }} form="search_form">
                                                    Zero image
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <button id="search_btn" class="btn btn-sm btn-warning"><i
                                                            class="fa fa-search" aria-hidden="true"></i> Search
                                                </button>
                                            </div>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->
                                <div class="panel-default panel-faq">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordion-three">
                                            <h4 class="panel-title" style="color: #009877">Location<span
                                                        class="pull-right"><i class="fa fa-minus" id="location_item"></i></span></h4>
                                        </a>
                                    </div><!-- panel-heading -->
                                    <div id="accordion-three" class="panel-collapse in collapse">
                                        <!-- panel-body -->
                                        <div class="panel-body">
                                            <ul>
                                                @foreach($regions as $region)
                                                    <li>
                                                        <a href="{{ url('/search/query?thana=&city=&region='.$region->id) }}"
                                                           style="color: #009877">{{ $region->title }}
                                                            ({{ \App\Ads::where('region_id', $region->id)->where('status', 1)->count() }}
                                                            )</a>
                                                    </li>
                                                    @if(request()->has('city') and request()->has('region') and request()->get('region'))
                                                        <ul style="margin-left: 5px">
                                                            @foreach($cities as $city)
                                                                <li>
                                                                    <a href="{{ url('/search/query?thana=&region='.$region->id.'&city='.$city->id) }}"
                                                                       style="color: #009877">{{ $city->title }}
                                                                        ({{ \App\Ads::where('city_id', $city->id)->where('status', 1)->count() }}
                                                                        )</a>
                                                                </li>
                                                                @if(request()->has('thana') and request()->has('city') and request()->get('city'))
                                                                    <ul style="margin-left: 5px">
                                                                        @foreach($thanas as $thana)
                                                                            <li>
                                                                                <a href="{{ url('/search/query?region='.$region->id.'&city='.$city->id.'&thana='.$thana->id) }}"
                                                                                    )/a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->

                            </div><!-- panel-group for desktop -->
                            
                            
                            
                            <!-- panel-group for mobile -->
                            <div style="display:none" class="panel-group panel-group-mobile" id="accordion">
                                <!-- panel -->
                                <div class="panel-default panel-faq">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordion-one-m" aria-expanded="false" class="collapsed">
                                            <h4 class="panel-title" style="color:#009877;">All Categories<span
                                                        class="pull-right"><i class="fa fa-minus" id="all_cat_item"></i></span></h4>
                                        </a>
                                    </div><!-- panel-heading -->

                                    <div id="accordion-one-m" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <!-- panel-body -->
                                        <div class="panel-body">
                                            <ul>
                                                @foreach(\App\Category::select('name','slug','icon', 'image')->where(['parent_id'=>0, 'status' => 1 ])->get() as $cat )
                                                    <li @if(str_replace('-', ' ', @$_REQUEST['main_category'] ) == strtolower($cat->name)) class="active" @endif >
                                                        <a style="color: #009877"
                                                           href="{{url('search/query?main_category='.urlencode($cat->slug))}}">{{ ucwords($cat->name) }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->
                                <!-- panel -->
                                @if( $setting->search_ads == 1 && $setting->search_ads_p == 'r' )
                                    <div class="inner-box">
                                        <!-- ads box -->
                                        {!! $setting->search_adsense !!}
                                    </div>
                                @endif
                                <div class="panel-default panel-faq">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordion-two-m" aria-expanded="false" class="collapsed">
                                            <h4 class="panel-title" style="color: #009877">Filters<span
                                                        class="pull-right"><i class="fa fa-minus" id="filter-item"></i></span></h4>
                                        </a>
                                    </div><!-- panel-heading -->
                                    <div id="accordion-two-m" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <!-- panel-body -->
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="price_range" class=" control-label"><b
                                                            style="color: #009877">Price ({{$setting->currency}}
                                                        )</b><span
                                                            class="font-normal text-muted f-s-12 clearfix"></span></label>
                                                <div class="">
                                                    <input type="text" id="price_range" form="search_form"
                                                           name="price_range">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="online" class="" style="color: #009877">User Online /
                                                    Offline</label>
                                                <select name="online" id="" class="form-control" form="search_form">
                                                    <option @if( $online == '') selected @endif  value="">All</option>
                                                    <option @if( $online == 1) selected @endif  value="1">Online
                                                    </option>
                                                    <option @if( $online == 2 ) selected @endif  value="2">Offline
                                                    </option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="img" style="color: #009877">
                                                    <input type="radio" name="image" value="1" id="img"
                                                           {{ ($image==1)? 'checked':'' }} form="search_form">
                                                    Image
                                                </label>
                                                <label for="no-img" class="m-l-5" style="color: #009877">
                                                    <input type="radio" name="image" value="0" id="no-img"
                                                           {{ ($image==0)? 'checked':'' }} form="search_form">
                                                    Zero image
                                                </label>
                                            </div>
                                            <div class="form-group">
                                                <button id="search_btn" class="btn btn-sm btn-warning"><i
                                                            class="fa fa-search" aria-hidden="true"></i> Search
                                                </button>
                                            </div>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->
                                <div class="panel-default panel-faq">
                                    <!-- panel-heading -->
                                    <div class="panel-heading">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#accordion-three-m" aria-expanded="false" class="collapsed">
                                            <h4 class="panel-title" style="color: #009877">Location<span
                                                        class="pull-right"><i class="fa fa-minus" id="location_item"></i></span></h4>
                                        </a>
                                    </div><!-- panel-heading -->
                                    <div id="accordion-three-m" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                        <!-- panel-body -->
                                        <div class="panel-body">
                                            <ul>
                                                @foreach($regions as $region)
                                                    <li>
                                                        <a href="{{ url('/search/query?thana=&city=&region='.$region->id) }}"
                                                           style="color: #009877">{{ $region->title }}
                                                            ({{ \App\Ads::where('region_id', $region->id)->where('status', 1)->count() }}
                                                            )</a>
                                                    </li>
                                                    @if(request()->has('city') and request()->has('region') and request()->get('region'))
                                                        <ul style="margin-left: 5px">
                                                            @foreach($cities as $city)
                                                                <li>
                                                                    <a href="{{ url('/search/query?thana=&region='.$region->id.'&city='.$city->id) }}"
                                                                       style="color: #009877">{{ $city->title }}
                                                                        ({{ \App\Ads::where('city_id', $city->id)->where('status', 1)->count() }}
                                                                        )</a>
                                                                </li>
                                                                @if(request()->has('thana') and request()->has('city') and request()->get('city'))
                                                                    <ul style="margin-left: 5px">
                                                                        @foreach($thanas as $thana)
                                                                            <li>
                                                                                <a href="{{ url('/search/query?region='.$region->id.'&city='.$city->id.'&thana='.$thana->id) }}"
                                                                                    )/a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                @endif
                                                            @endforeach
                                                        </ul>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div><!-- panel-body -->
                                    </div>
                                </div><!-- panel -->

                            </div><!-- panel-group -->
                        </div>
                    </div>




                    <!-- recommended-listings -->
                    <div class="col-sm-8 col-md-9">
                        <div class="section recommended-listings col-sm-8">
                            <!-- featured-top -->
                          <div class="featured-top" style="margin-top111: 55px;  margin-bottom: 10px;">
                                <h4 style="color: #009877; display: inline;">Search Result</h4>
                               
							<div style="float:right">

								<form  action="{{url('search/query')}}" method="get"  style="111: right;">
                            
                             <input style="width: 69%;float:left" type="text" name="keyword" class="form-control has-icon" style="display: inline-block;width:110px;" placeholder="Keyword" onchange="return false;" value="">
                                
                             
                              
                               
                             
                                
                             
                                    <button class="btn btn-warning btn-search_btn-block"  style="display: inline-block;

width: 72px;

padding: 9.6px 0px !important;

margin-top: -1px;

border: none !important;

box-shadow: none !important;"><i class="icon-search" style="color: black"></i><strong style="color: black">Find</strong></button>
                      
                      </form> 
     </div>



<!-- hhhhhhhhhhhhhhhhhhhhhhhh -->

       <?php
             
             if(count($urgent_ads)==0 &&  count($top_ads)==0 && count($bump_ads)==0 && count($non_boost_ads)==0 ){
                ?>

               <div class="text-center error">No results found </div>
                <?php

             }
       ?>






                        <div class="clearfix"></div>


                            </div><!-- featured-top -->




  







                        @if(count($urgent_ads) > 0)
                            
                            @foreach($urgent_ads as $v)
                                <?php
                                if (strlen($v->title) > 35) {
                                    $ad_title = substr($v->title, 0, 35) . '..';
                                } else {
                                    $ad_title = $v->title;
                                }
                                $user_type = \App\User::where('id', $v->user_id)
                                ->value('type');
                                ?>


                           
                                <!-- urgent top page price -->
                                    @if($v->f_type == 'top_page_price' || $v->f_type == 'urgent_top_price')


                                    

                                    <div class="item_indiv"  href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                    
                                        <div class="listings-item row">
                                            <!-- item-image -->
                                            <div class="item-image-box col-sm-4">
                                                

                                                <div class="item-image">
                                                   <!--  <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}"> -->
                                                        @if($v->f_type == 'urgent_top_price')
                                                            <span class="featured-listings urgent-hot">Urgent Hot</span>
                                                        @endif
                                                        @if($v->f_type == 'top_page_price')
                                                            <span class="featured-listings bg-featured">Featured</span>
                                                        @endif
                                                        <img src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                             alt="Image" class="img-responsive">
                                                    <!-- </a> -->
                                                </div><!-- item-image -->


                                            </div>
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a
                                                                href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         if($v->boost_accept){
                                                            echo '<span style="color:#009877;">&nbsp;Ad</span>';
                                                        } ?>


                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div>

                                    </div>
                                    @endif

                                    @if($v->f_type == 'urgent_price' || $v->f_type == '' || $v->f_type == 'home_page_price')

                                     <div class="item_indiv"  href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                    
                                        <div class="listings-item row" style="width: 550px;
                                            height: 138px;
                                            border: 1px solid #ffcb5f;
                                            margin-bottom: 12px;
                                            background-color: #f1de8e24;">
                                            <div class="item-image-box col-sm-4">
                                                <!-- item-image -->
                                                <div class="item-image">
                                                    @if($v->f_type == 'urgent_price')
                                                        <span class="featured-listings urgent">Urgent</span>
                                                    @endif
                                                    @if(isset($v->user->is_verified) && $v->user->is_verified==2)
                                                        <a href="javascript:void(0)" class="verified"
                                                           data-toggle="tooltip" data-placement="left" title=""
                                                           data-original-title="Verified"><i
                                                                    class="fa fa-check-square-o"></i></a>
                                                    @endif
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}"><img
                                                                src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                                alt="Image" class="img-responsive"></a>

                                                </div><!-- item-image -->
                                            </div><!-- item-image-box -->
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a style="color: #009877"
                                                                              href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         <?php

                                                       //echo " boost accepted = ".$v->boost_type_accepted;
                                                       $boost_type_accepted_name="";

                                                         if($v->boost_accept){
                                                             $boost_type_accepted=$v->boost_type_accepted;
                                                             $boost_type_accepted_name="";

                                                            if($boost_type_accepted=="topad"){
                                                                $boost_type_accepted_name="Top Ad";

                                                                //echo " boost_type_accepted_name topad = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="urgent"){
                                                                $boost_type_accepted_name="Urgent";

                                                                //echo " boost_type_accepted_name urg = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="dailybump"){
                                                                $boost_type_accepted_name="Daily bump";

                                                                //echo " boost_type_accepted_name daily = ".$boost_type_accepted_name;

                                                            }

                                                            }


                                                            ?>
                                                            <span style="color:#009877;">&nbsp;<?php echo $boost_type_accepted_name; ?></span>

                                                            <?php
                                                         ?>

                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div><!-- ad-listings -->

                                    </div>
                                @endif
                            @endforeach
                        @endif








                        @if(count($top_ads) > 0)
                            
                            @foreach($top_ads as $v)
                                <?php
                                if (strlen($v->title) > 35) {
                                    $ad_title = substr($v->title, 0, 35) . '..';
                                } else {
                                    $ad_title = $v->title;
                                }
                                $user_type = \App\User::where('id', $v->user_id)->value('type');
                                ?>
                                <!-- urgent top page price -->
                                    @if($v->f_type == 'top_page_price' || $v->f_type == 'urgent_top_price')

                                     <div class="item_indiv"  href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                    

                                        <div class="listings-item row" style="width: 550px;
                                            height: 138px;
                                            border: 1px solid #ffcb5f;
                                            margin-bottom: 12px;
                                            background-color: #f1de8e24;">
                                            <!-- item-image -->
                                            <div class="item-image-box col-sm-4">
                                                <div class="item-image">
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                                        @if($v->f_type == 'urgent_top_price')
                                                            <span class="featured-listings urgent-hot">Urgent Hot</span>
                                                        @endif
                                                        @if($v->f_type == 'top_page_price')
                                                            <span class="featured-listings bg-featured">Featured</span>
                                                        @endif
                                                        <img src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                             alt="Image" class="img-responsive">
                                                    </a>
                                                </div><!-- item-image -->
                                            </div>
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a
                                                                href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         if($v->boost_accept){
                                                            echo '<span style="color:#009877;">&nbsp;Ad</span>';
                                                        } ?>


                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div>

                                    </div>
                                    @endif

                                    @if($v->f_type == 'urgent_price' || $v->f_type == '' || $v->f_type == 'home_page_price')

                                    <div class="item_indiv"  href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                    
                                        <div class="listings-item row" style="width: 550px;
                                            height: 138px;
                                            border: 1px solid #ffcb5f;
                                            margin-bottom: 12px;
                                            background-color: #f1de8e24;">
                                            <div class="item-image-box col-sm-4">
                                                <!-- item-image -->
                                                <div class="item-image">
                                                    @if($v->f_type == 'urgent_price')
                                                        <span class="featured-listings urgent">Urgent</span>
                                                    @endif
                                                    @if(isset($v->user->is_verified) && $v->user->is_verified==2)
                                                        <a href="javascript:void(0)" class="verified"
                                                           data-toggle="tooltip" data-placement="left" title=""
                                                           data-original-title="Verified"><i
                                                                    class="fa fa-check-square-o"></i></a>
                                                    @endif
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}"><img
                                                                src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                                alt="Image" class="img-responsive"></a>

                                                </div><!-- item-image -->
                                            </div><!-- item-image-box -->
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a style="color: #009877"
                                                                              href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         <?php

                                                       //echo " boost accepted = ".$v->boost_type_accepted;
                                                       $boost_type_accepted_name="";

                                                         if($v->boost_accept){
                                                             $boost_type_accepted=$v->boost_type_accepted;
                                                             $boost_type_accepted_name="";

                                                            if($boost_type_accepted=="topad"){
                                                                $boost_type_accepted_name="Top Ad";

                                                                //echo " boost_type_accepted_name topad = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="urgent"){
                                                                $boost_type_accepted_name="Urgent";

                                                                //echo " boost_type_accepted_name urg = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="dailybump"){
                                                                $boost_type_accepted_name="Daily bump";

                                                                //echo " boost_type_accepted_name daily = ".$boost_type_accepted_name;

                                                            }

                                                            }


                                                            ?>
                                                            <span style="color:#009877;">&nbsp;<?php echo $boost_type_accepted_name; ?></span>

                                                            <?php
                                                         ?>

                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div><!-- ad-listings -->
                                    </div>
                                @endif
                            @endforeach
                        @endif






                        @if(count($bump_ads) > 0)
                            
                            @foreach($bump_ads as $v)
                                <?php
                                if (strlen($v->title) > 35) {
                                    $ad_title = substr($v->title, 0, 35) . '..';
                                } else {
                                    $ad_title = $v->title;
                                }
                                $user_type = \App\User::where('id', $v->user_id)->value('type');
                                ?>
                                <!-- urgent top page price -->
                                    @if($v->f_type == 'top_page_price' || $v->f_type == 'urgent_top_price')

                                    <div class="item_indiv"  href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                    
                                        <div class="listings-item row" style="width: 550px;
                                            height: 138px;
                                            border: 1px solid #ffcb5f;
                                            margin-bottom: 12px;
                                            background-color: #f1de8e24;">
                                            <!-- item-image -->
                                            <div class="item-image-box col-sm-4">
                                                <div class="item-image">
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                                        @if($v->f_type == 'urgent_top_price')
                                                            <span class="featured-listings urgent-hot">Urgent Hot</span>
                                                        @endif
                                                        @if($v->f_type == 'top_page_price')
                                                            <span class="featured-listings bg-featured">Featured</span>
                                                        @endif
                                                        <img src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                             alt="Image" class="img-responsive">
                                                    </a>
                                                </div><!-- item-image -->
                                            </div>
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a
                                                                href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         if($v->boost_accept){
                                                            echo '<span style="color:#009877;">&nbsp;Ad</span>';
                                                        } ?>


                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div>

                                    </div>
                                    @endif

                                    @if($v->f_type == 'urgent_price' || $v->f_type == '' || $v->f_type == 'home_page_price')

                                    <div class="item_indiv"  href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                    
                                        <div class="listings-item row" style="width: 550px;
                                            height: 138px;
                                            border: 1px solid #ffcb5f;
                                            margin-bottom: 12px;
                                            background-color: #f1de8e24;">
                                            <div class="item-image-box col-sm-4">
                                                <!-- item-image -->
                                                <div class="item-image">
                                                    @if($v->f_type == 'urgent_price')
                                                        <span class="featured-listings urgent">Urgent</span>
                                                    @endif
                                                    @if(isset($v->user->is_verified) && $v->user->is_verified==2)
                                                        <a href="javascript:void(0)" class="verified"
                                                           data-toggle="tooltip" data-placement="left" title=""
                                                           data-original-title="Verified"><i
                                                                    class="fa fa-check-square-o"></i></a>
                                                    @endif
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}"><img
                                                                src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                                alt="Image" class="img-responsive"></a>

                                                </div><!-- item-image -->
                                            </div><!-- item-image-box -->
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a style="color: #009877"
                                                                              href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         <?php

                                                       //echo " boost accepted = ".$v->boost_type_accepted;
                                                       $boost_type_accepted_name="";

                                                         if($v->boost_accept){
                                                             $boost_type_accepted=$v->boost_type_accepted;
                                                             $boost_type_accepted_name="";

                                                            if($boost_type_accepted=="topad"){
                                                                $boost_type_accepted_name="Top Ad";

                                                                //echo " boost_type_accepted_name topad = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="urgent"){
                                                                $boost_type_accepted_name="Urgent";

                                                                //echo " boost_type_accepted_name urg = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="dailybump"){
                                                                $boost_type_accepted_name="Daily bump";

                                                                //echo " boost_type_accepted_name daily = ".$boost_type_accepted_name;

                                                            }

                                                            }


                                                            ?>
                                                            <span style="color:#009877;">&nbsp;<?php echo $boost_type_accepted_name; ?></span>

                                                            <?php
                                                         ?>

                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div><!-- ad-listings -->

                                    </div>
                                @endif
                            @endforeach
                        @endif


                         @if(count($non_boost_ads) > 0)
                            
                         @foreach($non_boost_ads as $v)
                                <?php
                                if (strlen($v->title) > 35) {
                                    $ad_title = substr($v->title, 0, 35) . '..';
                                } else {
                                    $ad_title = $v->title;
                                }
                                $user_type = \App\User::where('id', $v->user_id)->value('type');
                                ?>



                                <!-- urgent top page price -->
                                    @if($v->f_type == 'top_page_price' || $v->f_type == 'urgent_top_price')

                                    <div class="item_indiv"   href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                        <div class="listings-item row" style="width: 550px;
                                            height: 138px;
                                            border: 1px solid #ffcb5f;
                                            margin-bottom: 12px;
                                            background-color: #f1de8e24;">
                                            <!-- item-image -->
                                            <div class="item-image-box col-sm-4">
                                                <div class="item-image">
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">
                                                        @if($v->f_type == 'urgent_top_price')
                                                            <span class="featured-listings urgent-hot">Urgent Hot</span>
                                                        @endif
                                                        @if($v->f_type == 'top_page_price')
                                                            <span class="featured-listings bg-featured">Featured</span>
                                                        @endif
                                                        <img src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                             alt="Image" class="img-responsive">
                                                    </a>
                                                </div><!-- item-image -->
                                            </div>
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a
                                                                href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         if($v->boost_accept){
                                                            echo '<span style="color:#009877;">&nbsp;Ad</span>';
                                                        } ?>


                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->

                                            
                                        </div>


                                    </div>


                                    @endif

                                    @if($v->f_type == 'urgent_price' || $v->f_type == '' || $v->f_type == 'home_page_price')

                                <div class="item_indiv2" href_tag="{{(( str_slug($v->id) ) )}}">
                                    <div class="item_indiv" href_tag="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}" >
                                        <div class="listings-item row" style="width: 550px;height: 138px;border-top: 1px solid #d4ded9;margin-bottom: 12px;padding-top: 3px;background:#f8f8f8;border-radius: 2px;">
                                            <div class="item-image-box col-sm-4">
                                                <!-- item-image -->
                                                <div class="item-image">
                                                    @if($v->f_type == 'urgent_price')
                                                        <span class="featured-listings urgent">Urgent</span>
                                                    @endif
                                                    @if(isset($v->user->is_verified) && $v->user->is_verified==2)
                                                        <a href="javascript:void(0)" class="verified"
                                                           data-toggle="tooltip" data-placement="left" title=""
                                                           data-original-title="Verified"><i
                                                                    class="fa fa-check-square-o"></i></a>
                                                    @endif
                                                    <a href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}"><img
                                                                src="{!! asset('assets/images/listings/'.(isset($v->ad_images[0]->image)? $v->ad_images[0]->image : 'empty.jpg').'') !!}"
                                                                alt="Image" class="img-responsive"></a>

                                                </div><!-- item-image -->
                                            </div><!-- item-image-box -->
                                            <!-- rending-text -->
                                            <div class="item-info col-sm-8">
                                                <!-- ad-info -->
                                                <!-- ad-info -->
                                                <div class="ad-info">
                                                    @if($setting->hide_price==0)
                                                        <h3 class="item-price pull-right">{{  $setting->currency_place == 'left' ? $setting->currency : ''  }}{{ number_format($v->price) }} {{  $setting->currency_place == 'right' ? $setting->currency : ''  }}</h3>
                                                    @endif
                                                    <h4 class="item-title"><a style="color: #009877"
                                                                              href="{{url('single/'.urlencode( str_slug($v->title.'-'.$v->id) ) )}}">{{ ucfirst($ad_title) }}</a>
                                                    </h4>
                                                    <div class="item-cat">
                                                        <span class="dated"><a href="javascript:void(0)"> <i
                                                                        class="fa fa-clock-o"></i> {{ $v->created_at->diffForHumans() }}</a></span>
                                                        - <span class="category">{{ $v->category->name }} </span>-
                                                        <span data-toggle="tooltip" data-placement="top" title=""
                                                              data-original-title="{{ floor($v->visit/2) }} Ads view "><i
                                                                    class="fa fa-eye"
                                                                    aria-hidden="true"> {{ floor($v->visit/2) }}</i></span>
                                                        - <span data-toggle="tooltip" data-placement="top" title=""
                                                                data-original-title="{{ floor($v->message) }} Message "><i
                                                                    class="fa fa-envelope-o"
                                                                    aria-hidden="true"> {{ floor($v->message) }}</i></span>

                                                    </div>
                                                    @foreach($v->ad_cf_data as $data)
                                                        @if($data->column_value!='')
                                                            <span class="label label-default">@if($data->icon!='' || $data->image !='')  {!! ($data->icon!='')? '<i class="'.$data->icon.'"></i>' : '<img src="'.asset('assets/images/c_icons/'.$data->image.'').'" height="38" width="30" >' !!} @endif {{ ucfirst(str_replace('_', ' ', $data->column_name)) }}: <span
                                                                        class="m-r-5 p-5"> {{$data->column_value}}</span></span>
                                                        @endif
                                                    @endforeach
                                                </div><!-- ad-info -->

                                                <!-- ad-meta -->
                                                <div class="ad-meta">
                                                    <div class="meta-content">
                                                        @if(!Auth::guest())
                                                            <a class="save-add"
                                                               data-action="{{ (count($v->save_add) > 0)? 'del': 'ins' }}"
                                                               data-id="{{$v->id}}" href="javascript:void(0)" title="">
                                                                @if(count($v->save_add) > 0)
                                                                    <span class="fa fa-star"></span>
                                                                    <span class="text">Add Saved</span>
                                                                @else
                                                                    <span class="fa fa-star-o"></span>
                                                                    <span class="text">Save Add</span>
                                                                @endif
                                                                <span class=" hidden fa fa-spinner fa-spin"></span>
                                                            </a>
                                                        @endif
                                                    </div>
                                                    <!-- item-info-right -->
                                                    <div class="user-option pull-right">
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=" ad's images"><i
                                                                    class="fa fa-camera"></i> {{ count($v->ad_images) }}
                                                        </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ ucwords( $v->city->title ) }}"><i
                                                                    class="fa fa-map-marker"></i> </a>
                                                        <a href="javascript:void(0)" data-toggle="tooltip"
                                                           data-placement="top" title=""
                                                           data-original-title="{{ $user_type == 'u' || $user_type == 'adm'? 'private' : 'Company' }}"><i
                                                                    class="fa {{ $user_type == 'u' || $user_type == 'adm'? 'fa-briefcase' : 'fa-user' }}"></i>
                                                        </a>

                                                        <a href="javascript:void(0)" class="saved-job">
                                                            @if($v->is_login)
                                                                <i data-toggle="tooltip" title="Online"
                                                                   class="fa fa-circle text-success"></i>
                                                            @else
                                                                <i data-toggle="tooltip" title="Offline"
                                                                   class="fa fa-circle text-danger"></i>
                                                            @endif
                                                        </a>

                                                         <?php

                                                       //echo " boost accepted = ".$v->boost_type_accepted;
                                                       $boost_type_accepted_name="";

                                                         if($v->boost_accept){
                                                             $boost_type_accepted=$v->boost_type_accepted;
                                                             $boost_type_accepted_name="";

                                                            if($boost_type_accepted=="topad"){
                                                                $boost_type_accepted_name="Top Ad";

                                                                //echo " boost_type_accepted_name topad = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="urgent"){
                                                                $boost_type_accepted_name="Urgent";

                                                                //echo " boost_type_accepted_name urg = ".$boost_type_accepted_name;

                                                            }elseif($boost_type_accepted=="dailybump"){
                                                                $boost_type_accepted_name="Daily bump";

                                                                //echo " boost_type_accepted_name daily = ".$boost_type_accepted_name;

                                                            }

                                                            }


                                                            ?>
                                                            <span style="color:#009877;">&nbsp;<?php echo $boost_type_accepted_name; ?></span>

                                                            <?php
                                                         ?>

                                                    </div><!-- item-info-right -->
                                                </div><!-- ad-meta -->
                                            </div><!-- item-info -->
                                        </div><!-- ad-listings -->
                                    </div></div>
                                @endif
                         @endforeach
                         @endif






                        <!-- pagination  -->
                            <div class="text-center">
                                <ul class="pagination ">
                                    @if(count($result) > 0)
                                        {{ $result->appends(request()->query())->links() }}
                                    @endif
                                </ul>
                            </div><!-- pagination  -->
                        </div>
                    

                     <div class="col-sm-3_00 col-md-3_00 col-sm-3" id="side_banner">
                          <br><br>
                
                                
                           <a href="https://www.grameenphone.com/"><img src="https://www.shakeout.org/2008/downloads/ShakeOut_BannerAds_DontFreak_160x600_v3.gif"></a>
                
                
                      </div>


                    </div>

                <!--  <div class="col-sm-3 col-md-3" id="side_banner"><br><br>
                
                                
                     <a href="https://www.grameenphone.com/"><img src="https://www.shakeout.org/2008/downloads/ShakeOut_BannerAds_DontFreak_160x600_v3.gif"></a>
                
                
                                </div>
 -->

                </div>
            </div>


<div class="main-container">
        <div class="container">
            <div class="col-md-12_0000">
               <div class="adengine">
                <?php $current_date = Date("Y-m-d H:i:s"); ?>
                @foreach($multiAdsShow as $value)
                @if($current_date <= $value->enddate)
                <a href="{{ $value->link }}" target="_blank"><img class="mySlides2" src="../public/upload/ads/{{ $value->image }}" style="width:100%; height: 150px;"></a>
                @endif
                @endforeach
                </div>
            </div>
        </div>
    </div>




        </div>
    </section>

    <input class="hidden form-control" type="text" id="search_prm"
           data-keyword="{{ (isset($_REQUEST['keyword']))? $_REQUEST['keyword'] : ''  }}"
           data-category="{{ (isset($_REQUEST['category']))? $_REQUEST['category'] : ''  }}"
           data-region="{{ (isset($_REQUEST['region']))? $_REQUEST['region'] : ''  }}">
    <script src="{{ asset('assets/plugins/ion-rangeslider/ion.rangeSlider.min.js') }}"></script>

    <script src="{{ asset('/assets/js/slick.js') }}"></script>

    <script>
        // category selectd
        //$(document).ready(function () {
        $('#category  option[value="{{$req_category}}"]').prop("selected", true);
        $('.price_sort_select  option[value="{{$req_price_sort}}"]').prop("selected", true);
        //});
        myIndex = 0;
        myIndex2 = 0;
        $(document).ready(function () {
            $("#price_range").ionRangeSlider({
                type: "double",
                grid: true,
                min: 0,
                max: 9000000,
                from: <?= $low_price ?>,
                to: <?= $high_price ?>,
                prefix: "{{$setting->currency}}",
                onChange: function(data){
                   
                   if(window.console){

                      console.log("  from = "+data.from+" to = "+data.to);

                   }

                }
            });
            //search btn

            $('#search_btn').click(function () {
                $('#loading').show();
                $('#search_form').submit();
            });

            $(".item_indiv").click(function(){
                 $elem = $(this);

                 var href_tag = $elem.attr('href_tag');

                 window.location.href=href_tag


            });

            $("#keyword").off('change');
            $("#keyword").off('click');
            $("#keyword").change(function(e){
                 
                 return false;

            });

            if($(window).width()<500){

                $("#filter-item").trigger('click');
                
                $("#location_item").trigger('click');

                $("#all_cat_item").trigger('click');

                
                
            }

            



/*****banner ad slide begins *******/


      $('.autoplay').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });

            // var myIndex = 0;
            // carousel();
            // carousel2();

       function carousel() {
              var i;
              var x = document.getElementsByClassName("mySlides");
              for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }

            if(window.console){
                console.log(" x = ");
                console.log(x);

            }

            if(window.console){

                console.log(" myindex  = "+ myIndex);

            }
            myIndex++;

             if(window.console){

                console.log(" x.length = "+x.length+" myindex  = "+ myIndex);

                console.log(" myIndex  > x. length = "+ (myIndex - x.length));

            }

            if (myIndex > x.length) {myIndex = 1} 

            if(window.console){

                console.log("..... myindex 2 = "+ myIndex2);
             

            }   
              x[myIndex-1].style.display = "block";  
           setTimeout(carousel, 7000); // Change image every 2 seconds
        }

            // var myIndex2 = 0;

            function carousel2() {
            

              var i;
              var x = document.getElementsByClassName("mySlides2");
              for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }

            /*if(window.console){
                console.log(" x = ");
                console.log(x);

            }

            if(window.console){

                console.log(" myindex 2 = "+ myIndex2);

            }*/

            myIndex2++;

            /*if(window.console){

                console.log(" x.length = "+x.length+" myindex 2 = "+ myIndex2);

                console.log(" myIndex 2 > x. length = "+ (myIndex2 - x.length));

            }*/
            if (myIndex2 > x.length) {myIndex2 = 1}    

             /*   if(window.console){

                console.log("..... myindex 2 = "+ myIndex2);
             

            }*/
              x[myIndex2-1].style.display = "block";  
  setTimeout(carousel2, 7000); // Change image every 2 seconds
}




/*****banner ad slide endss *******/






        });

        function sortPrice(v) {

            $('#loading').show();
            $('#price_sort').val(v);
            $('#search_form').submit();
            //console.log('ko');
        }

    </script>
    <script>
 myIndex = 0;
        $(document).ready(function () {
            $('.autoplay').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });

            
            carousel();

            function carousel() {
              var i;
              var x = document.getElementsByClassName("mySlides");
              for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }

            

            if(window.console){
                console.log|(" x ===  ");
                console.log(x);

            }
            if(x.length!=0){

            
            myIndex++;
            if (myIndex > x.length) {myIndex = 1}    
              x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 7000); // Change image every 2 seconds
  }
}
})
    </script>
    <script>
 myIndex2 = 0;
        $(document).ready(function () {
            $('.autoplay').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 2000,
            });

            
            carousel();

            function carousel() {
              var i;
              var x = document.getElementsByClassName("mySlides2");
              for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }

            

            if(window.console){
                console.log|(" x ===  ");
                console.log(x);

            }
            if(x.length!=0){

            
            myIndex2++;
            if (myIndex2 > x.length) {myIndex2 = 1}    
              x[myIndex2-1].style.display = "block";  
  setTimeout(carousel, 7000); // Change image every 2 seconds
  }
}
})
    </script>
    <script>
        var items = $('.item_indiv2');
items.sort(function(b, a){
    return +$(a).attr('href_tag') - +$(b).attr('href_tag');
});
    
items.appendTo('.section');
    </script>

@endsection

