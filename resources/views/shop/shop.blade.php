@extends('layouts.app')
@section('content')
<style type="text/css">
	
	body {
    background-color: #BDBDBD
}

.container {
    margin-top: 0px
}

.item {
    background:#29b76547;
    -webkit-box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
    box-shadow: 1px 1px 1px rgba(0, 0, 0, 0.1);
    padding: 20px;
    margin-bottom: 20px;
    overflow: hidden;
    position: relative;
    height: 300px;
    border: 2px solid #ffed92;
}

.item h4 {
    font-size: 2rem;
    margin-top: 38%;
    margin-left: 29%;
    color: #337ab7;
}

.text-primary {
    color: #ff6b6b !important
}

.item img {
    margin: 0 auto;
    display: inherit;
    width: 230px;
    height: 164px;

}

.overlay {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    right: 0;
    background: #29b76547;
    opacity: 0;
    -webkit-transition: all 0.3s;
    transition: all 0.3s
}
}

.overlay a {
    -webkit-transform: translateY(20px);
    transform: translateY(20px);
    -webkit-transition: all 0.5s;
    transition: all 0.5s
}

.btn {
    font-weight: 400;
    border: 1px solid transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    -webkit-transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
    transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out, -webkit-box-shadow 0.15s ease-in-out
}

.btn-dark {
    border-radius: 50px !important;
    padding-left: 30px !important;
    padding-right: 30px !important;
    margin-bottom: 10px
}

.btn-unique {
    color: #212529;
    background-color: #29b76547;
    border-color: #ff6b6b;
    color: #fff
}

.item:hover .overlay {
    opacity: 1
}

.item:hover .overlay a {
    -webkit-transform: translateY(0);
    transform: translateY(0);
    color: #fff;

    margin-top: 140px;
    margin-left: 125px;

}


</style>


<div class="container">
    <br>
    <br>
    <br>
    <br>
    <div class="row">
           
               @foreach($shop_cats as $value)
               
                
                <div class="col-md-4">
            <div class="item">

                <h4 style="color: #009877;">
                    <i class="fa fa-shopping-basket"></i>
                    <a href="shop/cat/<?php  echo $value->id;  ?>">
                    {{$value->category_name}}
                    </a>

                </h4>
               
            </div>
        </div> 
           
          @endforeach
        
        

    </div>
</div>
@endsection