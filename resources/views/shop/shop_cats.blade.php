@extends('admin.layout.app')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <li class="active"> Shop Product Add </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <h3 class="panel-title">Shop Categories</h3>
                            </div>
                            <div class="panel-body">

                               
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Category Name</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                            <?php 
                            $counter=0;
                              foreach($shop_cats as $cat){
                                ++$counter;

                            ?>
                            <tr>
                                <td><?php echo $counter; ?></td>
                                <td>{{ $cat->category_name }}</td>
                                
                                
                            </tr>
                              <?php
                                 }
                              ?>
                            </tbody>
                        </table>
                    </div>





                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
