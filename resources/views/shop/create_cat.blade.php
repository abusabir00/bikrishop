@extends('admin.layout.app')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <li class="active"> Shop Product Add </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <h3 class="panel-title">Create  Category</h3>
                            </div>
                            <div class="panel-body">

                                <form action="shop-cat-create-now" enctype="multipart/form-data" method="post" id="customForm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id">
                                    <div class="form-group">
                                        <label for=""> Category Name </label>
                                        <input type="text" class="form-control" id="category_name" name="category_name" required />
                                    </div>

                                   
                               



                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" name="done"> Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
