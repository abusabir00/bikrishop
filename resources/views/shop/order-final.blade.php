@extends('layouts.app')
@section('content')

<div class="jumbotron text-center" style="background-color: white;">
  <h1 class="display-3">Thank You For Your Order!</h1>
  <p class="lead"><strong style="color: #009877">Please check your email</strong> for Keep Look at Your Order</p>
  <hr>
  <p>
    Having trouble? <a href="{{ url('contact-us')}}">Contact us</a>
  </p>
  <p class="lead">
    <a class="btn btn-warning btn-sm" href="{{ url('/')}}" role="button" style="color: black !important;">Continue to homepage</a>

    <a class="btn btn-success btn-sm" href="{{ url('shop')}}" role="button" style="color: black !important;">Continue to Shop</a>
  </p>
</div>


@endsection