@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <h2>Check and decide</h2><br>
                         <form action="{{ url('edit-action' ) }}" method="POST" enctype="multipart/form-data">                           
                            {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{ $order_action->id }}" />
                            <label class="radio-inline btn btn-success">
                              <input type="radio" value="1" name="status">Accept
                            </label>
                            <label class="radio-inline btn btn-danger">
                              <input type="radio" value="2" name="status">Regect
                            </label>
                            <br><br>
                            <button type="submit" name="done" class="btn btn-primary">Submit</button>
                          </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection