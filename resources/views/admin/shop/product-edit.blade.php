@extends('admin.layout.app')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <li class="active"> Shop Product Add </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <h3 class="panel-title">Custom Page</h3>
                            </div>
                            <div class="panel-body">

                                <form action="{{ url('product-action' ) }}" enctype="multipart/form-data" method="POST" id="customForm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id" value="{{ $product_action->id }}">
                                    <div class="form-group">
                                        <label for=""> Product Name </label>
                                        <input type="text" class="form-control" id="title" name="productname" value="{{ $product_action->productname }}" requireD />
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Product Price </label>
                                        <input type="text" class="form-control" id="title" name="productprice"  value="{{ $product_action->productprice }}" requireD />
                                    </div>
                                     
                                     <div class="form-group">
                                         <label for=""> Product Stock </label><br>
                                        <label class="radio-inline btn btn-success">
                                        <input type="radio" value="0" name="productstatus">In-Stock
                                        </label>
                                       <label class="radio-inline btn btn-danger">
                                        <input type="radio" value="1" name="productstatus">Out-Stock
                                        </label>
                                        <label class="radio-inline btn btn-primary">
                                        <input type="radio" value="2" name="productstatus">Re-Stock
                                        </label>

                                     </div>

                                    <div class="form-group">
                                        <label for=""> Product Details </label>
                                        <textarea class="form-control" id="content" name="productdetails" required >{{ $product_action->productdetails }}</textarea>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" name="done"> Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
