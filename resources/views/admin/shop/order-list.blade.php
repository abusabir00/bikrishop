@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Customer Name</th>
                                <th>Customer Email</th>
                                <th>Mobile Number</th>
                                <th>Full Address</th>
                                <th>Order Id</th>
                                <th>Tranx Number</th>
                                <th>Order Status</th>
                                <th>Purchase Date</th>
                                <th>Status&nbsp;|&nbsp;Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($order_list as $value)    
                            <tr>
                              <th scope="row">{{$value->id}}</th>
                              <td>{{$value->productname}}</td>
                              <td>{{$value->fullname}}</td>
                              <td>{{$value->email}}</td>
                              <td>{{$value->mobilenumber}}</td>
                              <td>{{$value->fulladdress}}</td>
                              <td>{{$value->order_id}}</td>
                              <td>{{$value->tranxid}}</td>
                              <td>
                                  
                                  @if($value->status == 0)
                                  <span class="badge badge-warning">Pending</span>
                                @elseif($value->status == 1)
                                 <span class="badge badge-success">Active</span>
                                @elseif($value->status == 2)
                                <span class="badge badge-danger">Rejected</span>
                                @endif

                              </td>
                              <td>{{$value->purchasedate}}</td>
                              <td><a href="{{ url('order-check/'.$value->id) }}" class="btn btn-success">Action</a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection