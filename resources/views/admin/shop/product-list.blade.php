@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Product Price</th>
                                <th>Product Image</th>
                                <th>product Id</th>
                                <th>Product Status</th>
                                <th>Dated</th>
                                <th>Status&nbsp;|&nbsp;Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product_list as $value)    
                            <tr>
                              <th scope="row">{{$value->id}}</th>
                              <td>{{$value->productname}}</td>
                              <td>{{$value->productprice}}</td>
                              <td><img src="public/upload/shop/{{$value->image}}" alt="camera" style="height: 80px;"></td>
                              <td>0</td>
                              <td>
                                 
                                  @if($value->productstatus == 0)
                                  <span class="badge badge-success">In Stock</span>
                                @elseif($value->productstatus == 1)
                                 <span class="badge badge-danger">Out Stock</span>
                                @elseif($value->productstatus == 2)
                                <span class="badge badge-primary">Re Stock</span>
                                @endif
                               
                               </td>
                              <td>0</td>
                              <td><a href="{{ url('delete-product/'.$value->id) }}" class="btn btn-danger">Delete</a><a href="{{ url('edit-product/'.$value->id) }}" class="btn btn-success">Edit</a></td></td><td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection