@extends('admin.layout.app')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <li class="active"> Shop Product Add </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <h3 class="panel-title">Add Product</h3>
                            </div>
                            <div class="panel-body">

                                <form action="/dummyads/product-add/insert" enctype="multipart/form-data" method="post" id="customForm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id">

                                    <div class="form-group">
                                        <label for="category_id"> Category Name </label>
                                        

                                        <select id="category_id" name="category_id"   required="required">
                                            <option value="">Select a category</option>
                                            <?php
                                              foreach ($shop_cats as $cat) {
                                                  ?>
                                                    <option value="<?php echo $cat->id; ?>"><?php echo $cat->category_name; ?></option>


                                                  <?php  
                                                
                                              }


                                            ?>
                                            
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Product Name </label>
                                        <input type="text" class="form-control" id="title" name="productname" requireD />
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Product Price </label>
                                        <input type="text" class="form-control" id="title" name="productprice" requireD />
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Product Image </label>
                                        <input type="file" class="form-control" id="title" name="image[]" required="required" />
                                    </div>

                                <div class="form-group">
                                    <label for=""> Product Image </label>
                                    <input type="file" class="form-control" id="title" name="image[]" />
                                </div>
                                
                                <div class="form-group">
                                    <label for=""> Product Image </label>
                                    <input type="file" class="form-control" id="title" name="image[]" />
                                </div>
                                
                                <div class="form-group">
                                    <label for=""> Product Image </label>
                                    <input type="file" class="form-control" id="title" name="image[]" />
                                </div>


                                    <div class="form-group">
                                        <label for=""> Product Details </label>
                                        <textarea class="form-control" id="content" name="productdetails" required ></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" name="done"> Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection
