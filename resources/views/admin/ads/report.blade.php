@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Image</th>
                                
                                <th>Total Reports</th>
                                
                                
                                
                                
                            </tr>
                            </thead>
                            <tbody>
                                <?php

                                $counter=0;

                                foreach ($report_result as $report_result_indiv){
                                    ++$counter;

                                    ?>

                                    <tr>
                                        <td><?php echo $counter; ?></td>
                                        <td><a href="<?php echo url('/single/'.$report_result_indiv->title.'-'.$report_result_indiv->ad_id_found); ?>"><?php echo $report_result_indiv->title; ?></a></td>
                                        <td>

                                            <?php

                                            
                                                $result_image=Db::table("ads_images")
                                                ->where(['ad_id'=>$report_result_indiv->ad_id_found])
                                                ->orderby('id','ASC')
                                                ->first();
                                                
                                                
                                              if($result_image){
                                                  
                                                  $img=$result_image->image;
                                                  
                                              }else{
                                                  $img="empty.jpg";
                                              }
                                                  
                                                  ?>
                                                  <img  style="width:120px; height:40px;" src="<?php echo url('assets/images/listings/'. $img); ?>"  alt="image" /> 
                                                  
                                            
                                        </td>
                                        <td>
                                            <?php echo $report_result_indiv->total; ?>
                                        </td>

                                    </tr>

                                    <?php

                                }//end of for 

                                ?>
                            <tr>
                                <!-- <td colspan="4">No Record found yet.</td> -->

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      
        });
    
</script>
@endsection