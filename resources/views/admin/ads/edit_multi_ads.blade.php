@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads request edit</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
            @endif
            @if (session('error'))
            <div class="alert alert-danger">
                {{ session('error') }}
            </div>
            @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <form action="{{ url('multi-ads/update') }}" method="post" enctype="multipart/form-data"> 
                          {{ csrf_field() }}

                          <div class="form-group{{ $errors->has('enddate') ? ' has-error' : '' }}">
                            <div class="col-md-12">
                              <label for="enddate" class="control-label">Ads end date:</label>
                              <input id="enddate" type="date" class="form-control" name="enddate" required autocomplete="off">

                              @if ($errors->has('enddate'))
                              <span class="help-block">
                                <strong>{{ $errors->first('enddate') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>
                    <input type="hidden" name="id" value="{{ $multiAds->id }}">
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                        <div class="col-md-12">
                          <label for="status" class="control-label">Status:</label>
                          <select class="form-control" name="status" required>
                             <option value="0" @if($multiAds->status == 0) selected @endif>Inactive</option>
                             <option value="1" @if($multiAds->status == 1) selected @endif>Active</option>
                         </select>

                         @if ($errors->has('status'))
                         <span class="help-block">
                          <strong>{{ $errors->first('status') }}</strong>
                      </span>
                      @endif
                  </div>
              </div>

              <div class="form-group">
                <div class="col-md-12"><br>
                    <button class="btn btn-success" type="submit">Submit</button>
                    <a class="btn btn-danger" href="{{ url("multi-ads-request") }}">Back</a>
                </div>
            </div>
        </div>

    </form>
</div>
</div>
</div>
</div>
</div>
</div>

@endsection