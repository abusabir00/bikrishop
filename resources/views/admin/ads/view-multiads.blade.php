@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Blog List</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <label>Ads Image</label><br>
                        <img src="../../public/upload/ads/{{$multiAds->image}}" alt="camera" style="height: 80px;"><br><br>
                        <label>Redirect Link</label><br>
                        <p>{{$multiAds->link}}</p>
                        <label>Package Name</label><br>
                        <p>{{$multiAds->package}}</p><br>
                        <label>Ads Starting date</label><br>
                        <p>{{$multiAds->startdate}}</p>
                        <label>Ads Ending date</label><br>
                        <p>{{$multiAds->enddate}}</p><br>
                        <label>Tranx ID</label><br>
                        <h1>{{$multiAds->transectionId}}</h1>
                    </div>

                    <a class="btn btn-primary" href="{{ url('multi-ads-request') }}">Back</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection