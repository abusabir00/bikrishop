@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads request</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Campaign Name</th>
                                <th>URL</th>
                                <th>Image</th>
                                <th>Status</th>
                                <th>Bkash ID</th>
                                <th>Starting Date</th>
                                <th>Chosen Package</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">No Record found yet.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var t = $('#load_datatable').DataTable({
            "pageLength":25,
            "order": [[7, 'desc']],
            processing: true,
            serverSide: true,
            "initComplete": function (settings, json) {
            },
            ajax: "{!! url('load-ads-request-list') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'campaignname', name: 'campaignname'},
                {data: 'link', name: 'link'},
                {data: 'image', name: 'image'},
                {data: 'status', name: 'status'},
                {data: 'transectionId', name: 'transectionId'},
                {data: 'startdate', name: 'startdate'},
                {data: 'package', name: 'package'},
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]

            
        });

        t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

    });
</script>
@endsection