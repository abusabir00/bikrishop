@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads request</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>user</th>
                                <th>Ad Title</th>
                                <th>Image</th>
                                <th>type</th>
                                <th>Package</th>
                                <th>Trx ID</th>
                                
                                
                                <!-- <th>Description</th> -->
                                
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="4">No Record found yet.</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var t = $('#load_datatable').DataTable({
            "pageLength":25,
            "order": [[5, 'desc']],
            processing: true,
            serverSide: true,
            "initComplete": function (settings, json) {
            },
            ajax: "{!! url('load-boost-request-list') !!}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'user_id', name: 'user_id'},

                {data: 'title', name: 'title'},
                
                {data: 'image', name: 'image'},
                {data: 'boost_type', name: 'boost_type'},
                  {data: 'package_id', name: 'package_id'},

                {data: 'transaction_id', name: 'transaction_id'},
                
                
                // {data: 'description', name: 'description'},
                
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });

        t.on( 'order.dt search.dt', function () {
                t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                    cell.innerHTML = i+1;
                } );
            } ).draw();

    });
</script>
@endsection