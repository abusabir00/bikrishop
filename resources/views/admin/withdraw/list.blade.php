@extends('admin.layout.app')
@section('content')
    <style>
        .add_more {
            float: right;
            margin-bottom: 5px;
        }
    </style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li><a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="active"> Withdraw Request</li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <div class="card-box">
                    <div class="row">
                        <div class="col-xs-12 bg-white">
                            <table id="load_datatable"
                                   class="table table-colored table-inverse table-hover table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Requester name</th>
                                    <th>Requester balance</th>
                                    <th>Withdraw amount</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($withdraws as $withdraw)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $withdraw->user['name'] }}</td>
                                        <td>{{ $withdraw->user['affAmount']['amount'] }}</td>
                                        <td>{{ $withdraw->amount }}</td>
                                        <td>{{ $withdraw->created_at->format('d-m-Y') }}</td>
                                        <td>{{ ucfirst($withdraw->status) }}</td>
                                        <td>
                                            @if($withdraw->status == 'pending')
                                                <a class="btn btn-outline-primary"
                                                   href="{{ url('withdraw-action/'.$withdraw->id.'?status=complete') }}"
                                                   title="Complete"><i class="fa fa-check"></i></a>
                                                <a class="btn btn-outline-danger"
                                                   href="{{ url('withdraw-action/'.$withdraw->id.'?status=rejected') }}"
                                                   title="Reject"><i class="fa fa-trash-o"></i></a>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>

        $(document).ready(function () {


            $('#load_datatable').DataTable();

        });

    </script>
@endsection

