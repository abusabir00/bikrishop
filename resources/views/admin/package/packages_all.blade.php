@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Packages </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">

                    <div class="col-xs-12 bg-white">
                        <div id="result" class="text-center"></div>
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th> Name</th>
                                <th>Price</th>
                                <th>Quantity</th>
                                <th>Action</th>
                                
                            </tr>
                            </thead>
                            <tbody>
                             <?php
                               $counter=0;
                              
                            foreach($packages_all as $value){    
                              ++$counter;
                              ?>
                            <tr>
                              <th scope="row">{{$counter}}</th>
                              <td>{{$value->packagename}}</td>
                              <td>{{$value->packageprice}}</td>
                              <td>{{$value->packageqty }}</td>
                              <td><button class="btn_delete" data-id="{{$value->id}}" value="Delete">Delete</button></td>
                              
                              
                            </tr>
                            <?php
                             }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

<script type="text/javascript">

    $(document).ready(function(){


    
    $("#edit_aciton").change(function(){
    var status_id = $(this).val();
    var user_id = $('#edit_aciton').data('id');
    var _token = $('input[name="_token"]').val();

            $.ajax({
                type: "POST",
                url: "<?php echo url('package-status-update') ?>",
                dataType: "json",
                data: {_token : _token, status_id : status_id, user_id:user_id },
                success: function(result) {
                    if (result == 1) {
                        location.reload(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
            });

   }); // $("#edit_aciton").change(function(){



  $(".btn_delete").click(function(){
      
      var $btn_delete = $(this);
      var package_id = $btn_delete.data('id');
      var _token = $('input[name="_token"]').val();


      $.ajax({
         method:"POST",
         url:"delete_usual_package_now",
         data:{_token : _token, package_id:package_id},
         success: function(response){

            if(response.trim()=="success".trim()){

                $btn_delete.closest('tr').remove();

            }else{

                $("#result").html(response);


            }


         },
         error: function(errors){

         }

      });


  });


    });
   
</script>
@endsection

