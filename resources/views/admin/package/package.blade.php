@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Dashboard</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Customer Name</th>
                                <th>Customer Id</th>
                                <th>package Number</th>
                                <th>Purchase Date</th>
                                <th>Tranx Id</th>
                                <th>Ads Left</th>
                                <th>Status</th>
                                <th>&nbsp;|&nbsp;Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($package as $value)  
                                   
                            <tr>
                              <th scope="row">{{$value->id}}</th>
                              <td>{{$value->name}}</td>
                              <td>{{$value->user_id}}</td>
                              <td>
                                @if($value->package_id == 1)
                                  <span class="text-info">Gold</span>
                                @elseif($value->package_id == 2)
                                 <span class="text-success">Platinum</span>
                                @elseif($value->package_id == 3)
                                <span class="text-danger">Dimond</span>
                                @endif

                              </td>
                              <td>{{$value->updated_at}}</td>
                              <td>{{$value->transection}}</td>
                              <td>{{$value->post_qty}}</td>
                              <td>
                                @if($value->status == 1)
                                <span class="badge badge-info">Pending</span>
                                @elseif($value->status == 2)
                                <span class="badge badge-success">Active</span>
                                 @else
                                <span class="badge badge-danger">denied</span>
                                @endif
                              </td>
                              <td>
                                  <select class="form-control edit_aciton"  data-id="{{ $value->id }}">
                                      <option value="">Select</option>
                                      <option value="1" {{ ($value->status == 1) ? 'selected=selected' : '' }}>Inactive</option>
                                      <option value="2" {{ ($value->status == 2) ? 'selected=selected' : '' }}>Active</option>
                                      <option value="3" {{ ($value->status == 3) ? 'selected=selected' : '' }}>Denied</option>
                                  </select>
                              </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

<script type="text/javascript">

  $(document).ready(function(){

    $(".edit_aciton").change(function(){
    var status_id = $(this).val();
    var package_id = $(this).data('id');
    
    var _token = $('input[name="_token"]').val();

    if(status_id.trim() !="".trim()){

    

            $.ajax({
                type: "POST",
                url: "<?php echo url('package-status-update') ?>",
                dataType: "json",
                data: {_token : _token, status_id : status_id, package_id:package_id},
                success: function(result) {
                    if (result == 1) {
                        location.reload(true);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(errorThrown);
                },
            });

            }

   });

      });
   
</script>
@endsection

