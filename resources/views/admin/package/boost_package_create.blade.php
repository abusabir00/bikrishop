@extends('admin.layout.app')
@section('content')


<style>
    .label {
        margin-right: 2px;
    }
    .error{
        color:red;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <!-- <li class="active"> Shop Product Add </li> -->
                        <li class="active"> Create Package </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <!-- <h3 class="panel-title">Custom Page</h3> -->
                                <h3 class="panel-title">Create Package</h3>
                            </div>
                            <div class="panel-body">

                                @if($errors->any())
                                  {!! implode('', $errors->all('<div class="error">:message</div>')) !!}
                                @endif

                                <form action="package-create/insert_boost_package" enctype="multipart/form-data" method="post" id="customForm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id">

                                    <div class="form-group">
                                        <label for="boost_type"> Boost Type </label>

                                        <select class="form-control" id="boost_type" name="boost_type" required="required">
                                            <option value="">Select a boost type</option>
                                            <option value="urgent">Urgent</option>
                                            <option value="topad">Top Ad</option>
                                            <option value="dailybump">Daily bump</option>
                                            
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="package_name"> Package Name </label>
                                        <input type="text" class="form-control" id="package_name" name="package_name" value="{{old('package_name') ?? '' }}" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="package_price"> Package Price </label>
                                        <input type="text" class="form-control" id="package_price"  value="{{old('package_price') ?? '' }}" name="package_price" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="package_day"> Package Day </label>
                                        <input type="text" class="form-control" id="package_day" value="{{old('package_day') ?? '' }}" name="package_day" required />
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" name="done"> Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection