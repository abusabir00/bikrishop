@extends('admin.layout.app')
@section('content')


<style>
    .label {
        margin-right: 2px;
    }
    .error{
        color:red;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <!-- <li class="active"> Shop Product Add </li> -->
                        <li class="active"> Create Package </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <!-- <h3 class="panel-title">Custom Page</h3> -->
                                <h3 class="panel-title">Create Package</h3>
                            </div>
                            <div class="panel-body">

                                @if($errors->any())
                                  {!! implode('', $errors->all('<div class="error">:message</div>')) !!}
                                @endif

                                <form action="package-create/insert" enctype="multipart/form-data" method="post" id="customForm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id">
                                    <div class="form-group">
                                        <label for="packagename"> Package Name </label>
                                        <input type="text" class="form-control" id="packagename" name="packagename" value="{{old('packagename') ?? '' }}" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="packageprice"> Package Price </label>
                                        <input type="text" class="form-control" id="packageprice"  value="{{old('packageprice') ?? '' }}" name="packageprice" required />
                                    </div>

                                    <div class="form-group">
                                        <label for="packageqty"> Package Quantity </label>
                                        <input type="text" class="form-control" id="packageqty" value="{{old('packageqty') ?? '' }}" name="packageqty" required />
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" name="done"> Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



@endsection