@extends('admin.layout.app')
@section('content')
<style>
    .label {
        margin-right: 2px;
    }
</style>
    <div class="content-page">
        <!-- Start content -->
        <div class="content">

            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">Dashboard</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('/home') }}">Dashboard</a></li>
                        <li class="active"> Blog Post </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="col-xs-12">

                <div class="col-xs-12">
                    <div class="row">
                        <div class="panel panel-color panel-inverse">
                            <div class="panel-heading">
                                <h3 class="panel-title">Custom Page</h3>
                            </div>
                            <div class="panel-body">

                                <form action="dummyadsfinal/blog-post" enctype="multipart/form-data" method="post" id="customForm">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="id">
                                    <div class="form-group">
                                        <label for=""> Post Name </label>
                                        <input type="text" class="form-control" id="title" name="name" requireD />
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Post Title </label>
                                        <input type="text" class="form-control" id="title" name="title" requireD />
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Post date </label>
                                        <input type="date" class="form-control" id="title" name="date" requireD />
                                    </div>

                                    <div class="form-group">
                                        <label for=""> Post Image </label>
                                        <input type="file" class="form-control" id="title" name="image" />
                                    </div>
                                            

                                    <div class="form-group">
                                        <label for=""> Post Content </label>
                                        <textarea class="form-control" id="content" name="blogpost" required ></textarea>
                                    </div>

                                    <div class="form-group">
                                        <button class="btn btn-success" type="submit" name="done"> Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <script src="{!! asset('assets/js/ckeditor/ckeditor.js') !!}"></script>
    <script>

        $(document).ready(function(){

            // ck editor1
            var editor = CKEDITOR.replace('content',{
                allowedContent: true,
            });

           
        });
    </script>

@endsection
