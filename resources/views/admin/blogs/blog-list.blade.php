@extends('admin.layout.app')
@section('content')
<style>
    .add_more{float: right; margin-bottom: 5px;
    }
</style>
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="col-xs-12">
            <div class="page-title-box">
                <h4 class="page-title">Blog List</h4>
                <ol class="breadcrumb p-0 m-0">
                    <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                    <li class="active"> Ads </li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="col-xs-12">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
           @endif

            <div class="card-box">
                <div class="row">
                    <div class="col-xs-12 bg-white">
                        <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Blog Name</th>
                                <th>Blog Title</th>
                                <th>Blog Image</th>
                                <th>Post Date</th>
                             
                                <th>Status&nbsp;|&nbsp;Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($blog_list as $value)    
                            <tr>
                              <th scope="row">{{$value->id}}</th>
                              <td>{{$value->name}}</td>
                              <td>{{$value->title}}</td>
                              <td><img src="public/upload/ads/{{$value->image}}" alt="camera" style="height: 80px;"></td>
                              <td>{{$value->date}}</td>

                              <td><a href="{{ url('edit-blog/'.$value->id)}}" class="btn btn-success"> Edit</a>
                                
                              <a href="{{ url('delete-blog/'.$value->id)}}" class="btn btn-danger"> Delete</a>
                              </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection