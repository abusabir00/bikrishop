@extends('admin.layout.app')
@section('content')
    <link rel="stylesheet" href="{{ asset('admin_assets/css/custom.css') }}">

    <style>
        .add_more{float: right; margin-bottom: 5px; }
        .parent ul{
            /*display: none;*/
        }
    </style>

    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="col-xs-12">
                <div class="page-title-box">
                    <h4 class="page-title">{{ ( isset($thana))? 'Edit Thana' : 'Add Thana' }}</h4>
                    <ol class="breadcrumb p-0 m-0">
                        <li> <a href="{{ url('dashboard') }}">Dashboard</a></li>
                        <li class="active">{{ ( isset($thana))? 'Edit Thana' : 'Add Thana' }} </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="col-xs-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                <div class="card-box">
                    <div class="row">
                        <div class="col-xs-7 bg-white product-listing">
                           @if(isset($thana))
                                <a href="{{ url('thana') }}" class="waves-effect pull-right btn btn-success"><i class="fa fa-plus"></i><span> Thana</span> </a>
                            @endif
                            <form id="add_thana" method="post" role="form" enctype="multipart/form-data">
                                {{csrf_field()}}

                                <input type="hidden" name="id" value="{{ ( isset($thana) && $thana->id != '')? $thana->id: '' }}" id="id" >

                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Division:</label>
                                                <select name="division_id" id="division_id" class="form-control" required>
                                                    <option value="">Select Division</option>
                                                    @foreach($region as $value)
                                                        <option {{ ( isset($thana) && $thana->division_id == $value->id )? 'selected': '' }} value="{{ $value->id }}">{{ $value->title }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">District:</label>
                                                <select name="district_id" id="district_id" class="form-control" required>
                                                    <option value="">Select District</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="">Thana Title:</label>
                                                <input type="text" name="title" class="form-control" value="{{ ( isset($thana) && $thana->title != '')? $thana->title: '' }}" id="title"  required >
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="c_info"></div>
                                </div>
                            </form>

                            <!-- End list  -->
                        </div>
                    </div>
                </div>
                    <div class="card-box">
                        <div class="row">
                            <div class="col-xs-7 bg-white product-listing">
                                <table id="load_datatable" class="table table-colored table-inverse table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Division</th>
                                        <th>District</th>
                                        <th>Thana</th>
                                        <th>Dated</th>
                                        <th>Status&nbsp;|&nbsp;Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4">No Record found yet.</td>
                                    </tr>
                                    </tbody>
                                </table>
                                <!-- End list  -->
                            </div>
                        </div>
                    </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function(){

            $('#load_datatable').DataTable({
                "pageLength":25,
                "order": [[0, 'desc']],
                processing: true,
                serverSide: true,
                "initComplete": function (settings, json) {
                },

                ajax: {
                            url: "<?php echo url('load-thana'); ?>",
                            method: 'get',
                        },
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'division', name: 'division'},
                    {data: 'district', name: 'district'},
                    {data: 'thana', name: 'thana'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });

            //saving new user
            $("#add_thana").submit(function(){
                $('#loading').show();
                var data = new FormData(this);
                $.ajax({
                    url: "<?php echo url('thana'); ?>",
                    data: data,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success: function(result){
                        $('#loading').hide();
                        
                        if( result == 1 ){
                            swal("Success!", "Record added successfully.", "success");
                            $('input[name="title"]').val('');
                            refreshTable();
                        }else{
                            swal("Error!", 'Something went wrong!', "error");
                        }

                        if (result == 2) {
                            swal("Error!", 'This title already exists!', "error");
                            $('input[name="title"]').val('');
                        }
                    }
                });
                return false;
            });
        });

        $("#division_id").change(function(){
                var prev_dist_id = "<?php if (isset($thana)) echo $thana->district_id ?>";
                var division_id = $(this).val();
                $.ajax({
                    url: "<?php echo url('load-city-for-thana'); ?>",
                    data: {
                        division_id:division_id,
                        "_token": "{{ csrf_token() }}"
                    },
                    type: 'POST',
                    success: function(response){
                    var option = '<option value="">Select District</option>';
                    if (response.responseCode == 1) {
                        $.each(response.data, function (id, value) {
                            if (value.id == prev_dist_id) {
                                option += '<option value="' + value.id + '" selected>' + value.title + '</option>';
                            }else{
                                option += '<option value="' + value.id + '">' + value.title + '</option>';
                            }
                        });
                    }
                    $("#district_id").html(option);
                    $(self).next().hide();
                    }
                });
               
            });

         @if (!empty($thana))
         $('#division_id').trigger('change');
         @endif

    </script>
@endsection

