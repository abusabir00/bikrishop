@extends('layouts.app')
@section('content')
    <style>        .main-container{
            padding: 30px 0;
        }

        /**
          Component
          **/

        label {
            width: 100%;
        }

        .card-input-element {
            display: none;
        }

        .card-input {
            margin: 10px;
            padding: 00px;
        }

        .card-input:hover {
            cursor: pointer;
            border:  1px solid #009877;
        }

        .card-input-element:checked + .card-input {
            box-shadow: 0 0 1px 1px #2ecc71;
        }





    </style>
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('user.sidebar')

                <div class="col-sm-9 row page-content">
                    <div class="inner-box">
                        <h2 class="title-2"><i class="icon-docs"></i> Affiliation </h2>
                        <p>Use to refer new user and get BDT {{ \App\Setting::first()->affiliation_amount }} in your account</p>
                        <p>Use this code: <code>{{ Auth::user()->affiliation_code }}</code> or</p>
                        <p>Use this link: <code>{{ url('/register?code=') }}{{ Auth::user()->affiliation_code }}</code></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
