@extends('layouts.app')
@section('content')
<style>        .main-container{
	padding: 30px 0;
}

/**
  Component
  **/

  label {
  	width: 100%;
  }

  .card-input-element {
  	/*display: none;*/
  }

  .card-input {
  	margin: 10px;
  	padding: 00px;
  }

  .card-input:hover {
  	cursor: pointer;
  	border:  1px solid #009877;
  }

  .card-input-element:checked + .card-input {
  	box-shadow: 0 0 1px 1px #2ecc71;
  }

  #boost_packages_top_ad{
  	/*display: none;*/
  } 

  #topad_packages, #dailybump_packages{
  	display: none;
  }



</style>
<div class="main-container">
	<div class="container">
		<div class="row">
			@include('user.sidebar')

			<div class="col-sm-9 row page-content">
				<div class="inner-box">
					<h2 class="title-2"><i class="icon-docs"></i> Packages </h2>
					
					
					<h1 style="color: #009877;">Select packages</h1>
					<form action="{{ url('make-boost-request-now') }}" method="post" enctype="multipart/form-data"> 
                          {{ csrf_field() }}

                          <div class="row">

                          	<div class="form-group">
							<label for="" style="color: #009877;">SELECT A BOOST TYPE</label>
							
							<!-- <select id="boost_type" name="boost_type" required="required">
							                          		<option value="">Select a boost type</option>
							                          		<option value="urgent">urgent</option>
							                          		<option value="topad">Top Ad</option>
							                          		<option value="dailybump">Daily bump</option>
							                          	</select> -->

							<?php
							$counter=0;
							foreach ($all_packages as $package_indiv) {
								if($package_indiv->boost_type=="urgent"){
									++$counter;

								}
							}

							if($counter){
								?>

							<input type="radio" value="urgent" class="boost_type" name="boost_type">&nbsp;Urgent&nbsp;&nbsp;

								<?php

							}
							?>
                          	


							<?php
							$counter=0;
							foreach ($all_packages as $package_indiv) {
								if($package_indiv->boost_type=="topad"){
									++$counter;

								}
							}

							if($counter){
								?>

							<input type="radio" value="topad" class="boost_type" name="boost_type">&nbsp;Top Ad&nbsp;&nbsp;
								<?php

							}
							?>
                          	
                          	<?php
							$counter=0;
							foreach ($all_packages as $package_indiv) {
								if($package_indiv->boost_type=="dailybump"){
									++$counter;

								}
							}

							if($counter){
								?>

							<input type="radio" value="dailybump" class="boost_type" name="boost_type">&nbsp;Daily bump&nbsp;&nbsp;

								<?php

							}
							?>
                          	
                          	

						</div>
                          	
                          </div>
						<div class="row" id="boost_packages_top_ad">
							
							<div class="packages_group" id="urgent_packages">
		
							<?php
								foreach ($all_packages as $package_indiv) {
									if($package_indiv->boost_type=="urgent"){
									?>



							<div class="col-md-4 col-lg-4 col-sm-4">

								<label>
									<input type="radio" name="package_id" value="<?php echo $package_indiv->id; ?>" class="card-input-element" required />

									<div class="panel panel-default card-input">
										<div class="panel-heading"><?php echo $package_indiv->package_name; 	 ?></div>
										<div class="panel-body">
											<?php echo $package_indiv->package_day; 	 ?> day(s) / <h4 class="text-danger"><?php echo $package_indiv->package_price; 	 ?> Taka</h4>
										</div>
									</div>

								</label>

							</div>


									<?php

								}

								}// end foreach loop
							?>
							
							</div><!-- end of id urgent_packages -->





							<div class="packages_group" id="topad_packages">
		
							<?php
								foreach ($all_packages as $package_indiv) {
									if($package_indiv->boost_type=="topad"){
									?>



							<div class="col-md-4 col-lg-4 col-sm-4">

								<label>
									<input type="radio" name="package_id" value="<?php echo $package_indiv->id; ?>" class="card-input-element" required />

									<div class="panel panel-default card-input">
										<div class="panel-heading"><?php echo $package_indiv->package_name; 	 ?></div>
										<div class="panel-body">
											<?php echo $package_indiv->package_day; 	 ?> day(s) / <h4 class="text-danger"><?php echo $package_indiv->package_price; 	 ?> Taka</h4>
										</div>
									</div>

								</label>

							</div>


									<?php

								}

								}// end foreach loop
							?>
							
							</div><!-- end of id topad_packages -->





							<div class="packages_group" id="dailybump_packages">
		
							<?php
								foreach ($all_packages as $package_indiv) {
									if($package_indiv->boost_type=="dailybump"){
									?>



							<div class="col-md-4 col-lg-4 col-sm-4">

								<label>
									<input type="radio" name="package_id" value="<?php echo $package_indiv->id; ?>" class="card-input-element" required />

									<div class="panel panel-default card-input">
										<div class="panel-heading"><?php echo $package_indiv->package_name; 	 ?></div>
										<div class="panel-body">
											<?php echo $package_indiv->package_day; 	 ?> day(s) / <h4 class="text-danger"><?php echo $package_indiv->package_price; 	 ?> Taka</h4>
										</div>
									</div>

								</label>

							</div>


									<?php

								}

								}// end foreach loop
							?>
							
							</div><!-- end of id dailybump_packages -->


							<!-- <div class="col-md-4 col-lg-4 col-sm-4">
							
								<label>
									<input type="radio" name="package_id" value="1" class="card-input-element" required />
							
									<div class="panel panel-default card-input">
										<div class="panel-heading">Package A</div>
										<div class="panel-body">
											10 Ad Post / <h4 class="text-danger">10 Taka</h4>
										</div>
									</div>
							
								</label>
							
							</div>
							<div class="col-md-4 col-lg-4 col-sm-4">
							
								<label>
									<input type="radio" name="package_id" value="2" class="card-input-element" required/>
							
									<div class="panel panel-default card-input">
										<div class="panel-heading">Package B</div>
										<div class="panel-body">
											100 Ad Post / <h4 class="text-danger">100 Taka</h4>
										</div>
									</div>
								</label>
							
							</div>
							<div class="col-md-4 col-lg-4 col-sm-4">
							
							 							
								<label>
									<input type="radio" name="package_id" value="3" class="card-input-element" required/> 
							
									<div class="panel panel-default card-input">
										<div class="panel-heading">Package C</div>
										<div class="panel-body">
											1000 Ad Post / <h4 class="text-danger">1000 Taka</h4>
										</div>
									</div>
								</label>
							</div> -->

						</div>

						<div class="form-group">
							<label for="" style="color: #009877;">ENTER TRNX ID</label>
							<input type="text" class="form-control" name="transection" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Trnx Id"  required>
						</div>

						<input type="hidden" name="ad_id" value="<?php 
						echo request()->segment(count(request()->segments())); ?>" />

						<button class="btn btn-warning" type="submit">Purchase</button>

					</form>



				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function(){

       $(".boost_type").removeAttr('checked');
       $(".card-input-element").removeAttr("checked");

       $(".boost_type").click(function(){
          
          var $boost_type = $(this);
          if($boost_type.val().trim() == "urgent".trim()){
                  
                  $(".packages_group").hide();
                  $(".card-input-element:hidden").removeAttr("checked");
                  $(".card-input-element:hidden").removeAttr('required');
                  $("#urgent_packages").show();          	
                  $(".card-input-element").setAttr('required','required');
                  


          }else if($boost_type.val().trim() == "topad".trim()){
                  
                  $(".packages_group").hide();
                  $(".card-input-element:hidden").removeAttr("checked");
                  $(".card-input-element:hidden").removeAttr('required');
                  $("#topad_packages").show();          	
                  $(".card-input-element").setAttr('required','required');

          }else if($boost_type.val().trim() == "dailybump".trim()){
                  
                  $(".packages_group").hide();
                  $(".card-input-element:hidden").removeAttr("checked");
                  $(".card-input-element:hidden").removeAttr('required');
                  $("#dailybump_packages").show();          	
                  $(".card-input-element").setAttr('required','required');

          }

          /*{

            $(".card-input-element").removeAttr("checked");
            $(".card-input-element").removeAttr('required');
            $("#boost_packages_top_ad").hide();          	          	
          }*/
       

       });


	});

</script>
@endsection
