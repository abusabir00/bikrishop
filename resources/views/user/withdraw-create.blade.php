@extends('layouts.app')
@section('content')
    <style>        .main-container {
            padding: 30px 0;
        }

        /**
          Component
          **/

        label {
            width: 100%;
        }

        .card-input-element {
            display: none;
        }

        .card-input {
            margin: 10px;
            padding: 00px;
        }

        .card-input:hover {
            cursor: pointer;
            border: 1px solid #009877;
        }

        .card-input-element:checked + .card-input {
            box-shadow: 0 0 1px 1px #2ecc71;
        }


    </style>
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('user.sidebar')

                <div class="col-sm-9 row page-content">
                    <div class="inner-box">
                        <h2 class="title-2"><i class="fa fa-bank"></i> Withdraw Request </h2>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ url('withdraw-store') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group"><label for="exampleInputEmail1">Amount (Available: {{ $amount }})</label>
                                <input type="number" class="form-control" name="amount" id="exampleInputEmail1"
                                       placeholder="Enter amount" required min="1" max="{{ $amount }}">
                            </div>

                            <button class="btn btn-warning" type="submit">Withdraw</button>

                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
