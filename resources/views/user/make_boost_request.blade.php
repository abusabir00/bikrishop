@extends('layouts.app')
@section('content')
<style>        .main-container{
	padding: 30px 0;
}

/**
  Component
  **/

  label {
  	width: 100%;
  }

  .card-input-element {
  	/*display: none;*/
  }

  .card-input {
  	margin: 10px;
  	padding: 00px;
  }

  .card-input:hover {
  	cursor: pointer;
  	border:  1px solid #009877;
  }

  .card-input-element:checked + .card-input {
  	box-shadow: 0 0 1px 1px #2ecc71;
  }

  #boost_packages_top_ad{
  	/*display: none;*/
  } 

  #topad_packages, #dailybump_packages{
  	display: none;
  }



</style>
<div class="main-container">
	<div class="container">
		<div class="row">
			@include('user.sidebar')

			<div class="col-sm-9 row page-content">
				<div class="inner-box">
					<h2 class="title-2"><i class="icon-docs"></i> Packages </h2>
					
					
					<h1 style="color: #009877;">Select packages</h1>
					<form action="{{ url('make-boost-request-now') }}" method="post" enctype="multipart/form-data"> 
                          {{ csrf_field() }}



                        <?php
							$counter=0;
							foreach ($all_packages as $package_indiv) {
								if($package_indiv->boost_type=="urgent"){
									++$counter;

								}
							}

							if($counter){
						?>
						<div class="row">
							<div class="panel-group driving-license-settings" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
												<input type="checkbox" value="urgent" class="boost_type package_calculation" name="urgent_boost_type"> Urgent	
											</a>
										  </h4>
									</div>
									<div id="collapseOne" class="panel-collapse collapse in">
										<div class="panel-body">
											<?php
												foreach ($all_packages as $package_indiv) {
													if($package_indiv->boost_type=="urgent"){
											?>

													<div class="col-md-4 col-lg-4 col-sm-4">

														<label>
															<input type="radio" name="urgent_packages" value="<?php echo $package_indiv->id; ?>" class="card-input-element package_calculation" amount="<?php echo $package_indiv->package_price; ?>" />

															<div class="panel panel-default card-input">
																<div class="panel-heading"><?php echo $package_indiv->package_name; 	 ?></div>
																<div class="panel-body">
																	<?php echo $package_indiv->package_day; 	 ?> day(s) / <h4 class="text-danger"><?php echo $package_indiv->package_price; 	 ?> Taka</h4>
																</div>
															</div>

														</label>

													</div>

											<?php 
												} 
											}// end foreach loop
											?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php } ?>


						<?php
							$counter=0;
							foreach ($all_packages as $package_indiv) {
								if($package_indiv->boost_type=="topad"){
									++$counter;

								}
							}

							if($counter){
						?>
						<div class="row">
							<div class="panel-group driving-license-settings" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
												<input type="checkbox" value="topad" class="top_boost_type package_calculation" name="top_boost_type"> Top Ad	
											</a>
										  </h4>
									</div>
									<div id="collapseTwo" class="panel-collapse collapse in">
										<div class="panel-body">
											<?php
												foreach ($all_packages as $package_indiv) {
													if($package_indiv->boost_type=="topad"){
											?>

													<div class="col-md-4 col-lg-4 col-sm-4">

														<label>
															<input type="radio" name="top_packages" value="<?php echo $package_indiv->id; ?>" class="card-input-element package_calculation" amount="<?php echo $package_indiv->package_price; ?>" />

															<div class="panel panel-default card-input">
																<div class="panel-heading"><?php echo $package_indiv->package_name; 	 ?></div>
																<div class="panel-body">
																	<?php echo $package_indiv->package_day; 	 ?> day(s) / <h4 class="text-danger"><?php echo $package_indiv->package_price; 	 ?> Taka</h4>
																</div>
															</div>

														</label>

													</div>

											<?php 
												} 
											}// end foreach loop
											?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php } ?>

						<?php
							$counter=0;
							foreach ($all_packages as $package_indiv) {
								if($package_indiv->boost_type=="dailybump"){
									++$counter;

								}
							}

							if($counter){
						?>
						<div class="row">
							<div class="panel-group driving-license-settings" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										  <h4 class="panel-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
												<input type="checkbox" value="dailybump" class="boost_type package_calculation" name="bump_boost_type"> Daily Bump	
											</a>
										  </h4>
									</div>
									<div id="collapseThree" class="panel-collapse collapse in">
										<div class="panel-body">
											<?php
												foreach ($all_packages as $package_indiv) {
													if($package_indiv->boost_type=="dailybump"){
											?>

													<div class="col-md-4 col-lg-4 col-sm-4">

														<label>
															<input type="radio" name="bump_packages" value="<?php echo $package_indiv->id; ?>" class="card-input-element package_calculation" amount="<?php echo $package_indiv->package_price; ?>" />

															<div class="panel panel-default card-input">
																<div class="panel-heading"><?php echo $package_indiv->package_name; 	 ?></div>
																<div class="panel-body">
																	<?php echo $package_indiv->package_day; 	 ?> day(s) / <h4 class="text-danger"><?php echo $package_indiv->package_price; 	 ?> Taka</h4>
																</div>
															</div>

														</label>

													</div>

											<?php 
												} 
											}// end foreach loop
											?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<?php } ?>

						<div class="row">
			                 <div class="col-lg-4 col-sm-5">
			                 </div>
			                 
			             </div>

			             <div class="row">
							<div class="panel-group driving-license-settings" id="accordion">
								<div class="panel panel-default">

									<div id="" class="">
										<div class="panel-body">
											<div class="col-lg-8 col-sm-5 ml-auto">
							                     <table class="table table-clear">
							                         <tbody>
							                             <tr>
							                                 <td class="left">
							                                     <strong class="text-dark">Urgent</strong>
							                                 </td>
							                                 <td class="right">
							                                 	<input type="number" name="urgent_amount" value="00" readonly>
							                                 </td>
							                             </tr>
							                             <tr>
							                                 <td class="left">
							                                     <strong class="text-dark">Top Ad </strong>
							                                 </td>
							                                 <td class="right">
							                                 	<input type="number" name="top_amount" value="00" readonly>
							                                 </td>
							                             </tr>
							                             <tr>
							                                 <td class="left">
							                                     <strong class="text-dark">Daily Bump</strong>
							                                 </td>
							                                 <td class="right">
							                                 	<input type="number" name="bump_amount" value="00" readonly>
							                                 </td>
							                             </tr>
							                             <tr>
							                                 <td class="left">
							                                     <strong class="text-dark">Total</strong> </td>
							                                 <td class="right">
							                                     <strong class="text-dark">
							                                     	<input type="number" name="total_amount" value="0" readonly>
							                                     </strong>
							                                 </td>
							                             </tr>
							                         </tbody>
							                     </table>
							                 </div>
											
										</div>
									</div>
								</div>
							</div>
						</div>


						<div class="form-group">
							<label for="" style="color: #009877;">ENTER TRNX ID</label>
							<input type="text" class="form-control" name="transection" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Trnx Id"  required>
						</div>

						<input type="hidden" name="ad_id" value="<?php 
						echo request()->segment(count(request()->segments())); ?>" />

						<button class="btn btn-warning" type="submit">Purchase</button>

					</form>



				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@section('js')

	<script type="text/javascript">
		// Package Input Requrement Management
		$( document ).ready(function() {
			$(':input[type="submit"]').prop('disabled', true);
			//Function for calculation
			$(".package_calculation").click(function(){
				//Set valu by checkbox
				var type = $(this).val();
				var is_check = $(this).prop("checked");
				if(type == 'urgent' && is_check == true){ 
					$("input[name='urgent_packages']").prop('checked', true); 
				}
				if(type == 'topad' && is_check == true){ 
					$("input[name='top_packages']").prop('checked', true); 
				}
				if(type == 'dailybump' && is_check == true){ 
					$("input[name='bump_packages']").prop('checked', true); 
				}

				if(type == 'urgent' && is_check == false){
					$("input[name='urgent_packages']").prop('checked', false);
				}
				if(type == 'topad' && is_check == false){
					$("input[name='top_packages']").prop('checked', false);
				}
				if(type == 'dailybump' && is_check == false){
					$("input[name='bump_packages']").prop('checked', false);
				}
				//get Selected Amount
				var urgent = $("input[name='urgent_packages']:checked").attr('amount'); 
				var urgent = (typeof urgent === 'undefined') ? '00' : urgent;
				var top = $("input[name='top_packages']:checked").attr('amount');
				var top = (typeof top === 'undefined') ? '00' : top;
				var bump = $("input[name='bump_packages']:checked").attr('amount');
				var bump = (typeof bump === 'undefined') ? '00' : bump;
				//set package amount value
				var total = parseInt(urgent) + parseInt(top) + parseInt(bump);
				var total = (typeof total === 'undefined') ? '0' : total;
				$("input[name=urgent_amount]").val(urgent);
				$("input[name=top_amount]").val(top);
				$("input[name=bump_amount]").val(bump);
				$("input[name=total_amount]").val(total);
				//set submit button
				if(total != '0'){
					$(':input[type="submit"]').prop('disabled', false);
				}else{ $(':input[type="submit"]').prop('disabled', true); }
					
			});

		});

		


		// Collaps script
		$('.collapse').collapse();
		$('.panel-heading h4 a input[type=checkbox]').on('click', function(e) {
		    e.stopPropagation();
		    $(this).parent().trigger('click');   // <---  HERE
		});

		$('#collapseOne').on('show.bs.collapse', function(e) {
		    if(!$('.panel-heading h4 a input[type=checkbox]').is(':checked'))
		    {
		        return false;
		    }
		});
		$('#collapseTwo').on('show.bs.collapse', function(e) {
		    if(!$('.panel-heading h4 a input[type=checkbox]').is(':checked'))
		    {
		        return false;
		    }
		});
		$('#collapseThree').on('show.bs.collapse', function(e) {
		    if(!$('.panel-heading h4 a input[type=checkbox]').is(':checked'))
		    {
		        return false;
		    }
		});
	</script>


@endsection
