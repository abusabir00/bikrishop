@extends('layouts.app')

@section('content')

    <style>

        .main-container {

            padding: 30px 0;

        }

        #preview_profile {

            display: block;

            height: 60px;

            margin-bottom: 10px;

        }

        .userImg {
            height: 55px;
        }


    </style>




    <div class="main-container">

        <div class="container">

            <div class="row">

                @include('user.sidebar')


                <div class="col-sm-9 row page-content">

                    <div class="inner-box">
                        @if(\Illuminate\Support\Facades\Session::has('message'))
                            <div class="alert alert-danger">
                                <p>{{ \Illuminate\Support\Facades\Session::get('message') }}</p>
                            </div>
                            @endif
                        @if(\Illuminate\Support\Facades\Session::has('success_message'))
                            <div class="alert alert-success">
                                <p>{{ \Illuminate\Support\Facades\Session::get('success_message') }}</p>
                            </div>
                            @endif
                        <div class="row">
                            <div class="col-md-4">
                                <h2 class="title-2"><i class="fa fa-bank"></i> Withdraw List </h2>
                            </div>
                            <div class="col-md-4">
                                <p>Available Balance: {{ $avAmount->amount ?? 0 }}</p>
                            </div>
                            <div class="col-md-4">
                                @if( $avAmount && $avAmount->amount > 0)
                                    <a href="{{ url('/withdraw/create') }}" class="btn btn-primary-dark float-right"><i
                                                class="fa fa-plus"></i> Add New Request</a>
                                @endif
                            </div>
                        </div>

                        <div class="table-responsive">

                            <table id="withdraw-table"
                                   class="table table-striped table-bordered add-manage-table table demo footable-loaded footable">

                                <thead>

                                <tr>

                                    <th>#</th>

                                    <th>Amount</th>

                                    <th>Status</th>

                                    <th>Created At</th>

                                </tr>

                                </thead>

                                <tbody>
                                @foreach($withdrawAmounts as $withdrawAmount)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ $withdrawAmount->amount }}</td>
                                        <td>{{ ucfirst($withdrawAmount->status) }}</td>
                                        <td>{{ $withdrawAmount->created_at->format('d-m-Y') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>



    <script>

        $(document).ready(function () {


            $('#withdraw-table').DataTable();

        });

    </script>





@endsection

