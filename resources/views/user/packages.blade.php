@extends('layouts.app')
@section('content')
<style>        .main-container{
	padding: 30px 0;
}

/**
  Component
  **/

  label {
  	width: 100%;
  }

  .card-input-element {
  	display: none;
  }

  .card-input {
  	margin: 10px;
  	padding: 00px;
  }

  .card-input:hover {
  	cursor: pointer;
  	border:  1px solid #009877;
  }

  .card-input-element:checked + .card-input {
  	box-shadow: 0 0 1px 1px #2ecc71;
  }





</style>
<div class="main-container">
	<div class="container">
		<div class="row">
			@include('user.sidebar')

			<div class="col-sm-9 row page-content">
				<div class="inner-box">
					<h2 class="title-2"><i class="icon-docs"></i> Packages </h2>
					<h3 class="title-2">

						<br/>
						<br/>
						<br/>
						<br/>
						<br/>

						<?php

						$remaining_ads=$total_ads-$ads_given;

						if($remaining_ads<0){

							$remaining_ads=0;

						}
						

					   echo " Ads given so far : ".$ads_given.", Remaining ads : ".
					   $remaining_ads;
					?>
					</h3>
					
					<h1 style="color: #009877;">Select packages</h1>
					<form action="{{ url('package-store') }}" method="post" enctype="multipart/form-data"> 
                          {{ csrf_field() }}
						<div class="row">

							<?php

  								foreach ($all_packages as $package_indiv) {
  									?>


							<div class="col-md-4 col-lg-4 col-sm-4">

								<label>
									<input type="radio" name="package_id" value="<?php echo $package_indiv->id; ?>" class="card-input-element" required />

									<div class="panel panel-default card-input">
										<div class="panel-heading"><?php echo $package_indiv->packagename; 	 ?></div>
										<div class="panel-body">
											<?php echo $package_indiv->packageqty; 	 ?> Ad Post / <h4 class="text-danger"><?php echo $package_indiv->packageprice; 	 ?> Taka</h4>
										</div>
									</div>

								</label>

							</div>



  									<?php
  								}
							?>

							<!-- <div class="col-md-4 col-lg-4 col-sm-4">
							
								<label>
									<input type="radio" name="package_id" value="1" class="card-input-element" required />
							
									<div class="panel panel-default card-input">
										<div class="panel-heading">Package A</div>
										<div class="panel-body">
											10 Ad Post / <h4 class="text-danger">10 Taka</h4>
										</div>
									</div>
							
								</label>
							
							</div> -->
							<!-- <div class="col-md-4 col-lg-4 col-sm-4">
							
								<label>
									<input type="radio" name="package_id" value="2" class="card-input-element" required/>
							
									<div class="panel panel-default card-input">
										<div class="panel-heading">Package B</div>
										<div class="panel-body">
											100 Ad Post / <h4 class="text-danger">100 Taka</h4>
										</div>
									</div>
								</label>
							
							</div>
							<div class="col-md-4 col-lg-4 col-sm-4">
							
								<label>
									<input type="radio" name="package_id" value="3" class="card-input-element" required/>
							
									<div class="panel panel-default card-input">
										<div class="panel-heading">Package C</div>
										<div class="panel-body">
											1000 Ad Post / <h4 class="text-danger">1000 Taka</h4>
										</div>
									</div>
								</label>
							</div> -->

						</div>

						<div class="form-group">
							<label for="" style="color: #009877;">ENTER TRNX ID</label>
							<input type="text" class="form-control" name="transection" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Trnx Id">
						</div>

						<button class="btn btn-warning" type="submit">Purchase</button>

					</form>



				</div>
			</div>
		</div>
	</div>
</div>
@endsection
