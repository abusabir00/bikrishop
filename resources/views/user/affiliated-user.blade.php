@extends('layouts.app')
@section('content')
    <style>        .main-container{
            padding: 30px 0;
        }

        /**
          Component
          **/

        label {
            width: 100%;
        }

        .card-input-element {
            display: none;
        }

        .card-input {
            margin: 10px;
            padding: 00px;
        }

        .card-input:hover {
            cursor: pointer;
            border:  1px solid #009877;
        }

        .card-input-element:checked + .card-input {
            box-shadow: 0 0 1px 1px #2ecc71;
        }





    </style>
    <div class="main-container">
        <div class="container">
            <div class="row">
                @include('user.sidebar')

                <div class="col-sm-9 row page-content">
                    <div class="inner-box">
                        <h2 class="title-2"><i class="icon-user"></i> Affiliated Users </h2>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Created At</th>
                                <th>Profile Link</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach(auth()->user()->affUsers as $user)
                            @php
                             
                            @endphp
                            <tr>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->created_at->format('F d, Y') }}</td>
                                <td><a href="user-ads/<?php echo  $user->id; ?>">Profile Link</a> </td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
