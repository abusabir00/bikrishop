@extends('layouts.app')
@section('content')

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<style>
body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>

<style type="text/css">
  
  html, body{
    /*font-size:0.91rem*/
    font-size: 14px !important;
  }
</style>
<body class="w3-light-grey">

<!-- w3-content defines a container for fixed size centered content, 
and is wrapped around the whole page content, except for the footer in this example -->
<div class="w3-content" style="max-width:1400px">

<!-- Header -->
<header class="w3-container w3-center w3-padding-32"  style="margin-top:100px;"> 
  <h1><b>DUMMY ADS<span class="w3-tag">BLOG</span></b></h1>
</header>

<!-- Grid -->
<div class="w3-row">

<!-- Blog entries -->
<div class="w3-col l8 s12">
  <!-- Blog entry -->
  @foreach($blog as $value)
  <div class="w3-card-4 w3-margin w3-white">
    <img src="public/upload/ads/{{$value->image}}" alt="Nature" style="width:100%; height: 300px;">
    <div class="w3-container">
      <h3><b>{{$value->name}}</b></h3>
      <h5>{!! $value->title !!}, <span class="w3-opacity">{{$value->date}}</span></h5>
    </div>

    <div class="w3-container">
      <p>{!! $value->post !!}</p>
      <div class="w3-row">
        <div class="w3-col m8 s12">
          <a href="{{ url('blog-single/'.$value->id)}}"><button class="w3-button w3-padding-large w3-white w3-border" ><b>READ MORE »</b></button></a>
        </div><br>
        <div class="w3-col m4 w3-hide-small">
          <p><span class="w3-padding-large w3-right"><span class="w3-tag">DUMMY ADS</span></span></p>
        </div>
      </div>
    </div>
  </div>
  <hr>
  @endforeach


</div>

<!-- Introduction menu -->
<div class="w3-col l4">

  <!-- Posts -->
  <div class="w3-card w3-margin">
    <div class="w3-container w3-padding">
      <h4>Popular Posts</h4>
    </div>
    <ul class="w3-ul w3-hoverable w3-white">

      @foreach($sideblog as $result)
      <li class="w3-padding-16">
        <img src="public/upload/ads/{{$result->image}}" alt="Image" class="w3-left w3-margin-right" style="width:50px">
        <span class="w3-large">{{$result->name}}</span><br>
        <span>{{$result->title}}</span>
      </li>
      @endforeach
    </ul>
  </div>
  <hr> 
 
  
<!-- END Introduction Menu -->
</div>

<!-- END GRID -->
</div><br>

<!-- END w3-content -->
</div>

<!-- Footer -->
@endsection
