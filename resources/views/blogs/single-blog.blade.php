@extends('layouts.app')
@section('content')

<div class="container">
	<br><br>
     
	<img src="../public/upload/ads/{{$blog->image}}" alt="Nature" style="width:100%; height: 300px;"><br><br>
	<h3 style="text-align: center;">{{$blog->name}}</h3>
    <br><br>
    <p class="text-break" style="text-align: justify; font-size: 18px;">{!! $blog->post !!}</p>
       
    
    <br><br><br>

</div>

@endsection