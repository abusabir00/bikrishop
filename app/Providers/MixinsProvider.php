<?php

namespace App\Providers;

use App\Mixins\StrMixins;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class MixinsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        Str::mixin(new StrMixins(), false);
    }
}
