<?php


namespace App\Mixins;


class StrMixins
{
    public function makePin()
    {
        return function ($length = 5) {
            $min = 1;
            $max = 1;
            for ($i = 2; $i <= $length; $i++) {
                $min = $min * 10;
                $max = $max * 10;
            }
            $max = (($max * 10) - 1);
            return mt_rand($min, $max);
        };
    }

}