<?php
namespace App;
use Illuminate\Notifications\Notifiable;

use Illuminate\Foundation\Auth\User as Authenticatable;
class User extends Authenticatable
{    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'plain_password',
        'password',
        'type',
        'phone',
        'gender',
        'zip',
        'address',
        'telephone',
        'fax',
        'vat',
        'affiliated_user',
        'affiliation_code'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function affby()
    {
        return $this->belongsTo(User::class, 'affiliated_user', 'id');
    }


    public function affAmount()
    {
        return $this->hasOne(AffiliationAmount::class, 'user_id', 'id');
    }

    public function affUsers(){
        return $this->hasMany(User::class, 'affiliated_user', 'id');
    }
}
