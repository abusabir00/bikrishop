<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
  
    protected $table = 'shop';

    protected $guarded=[];

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [];

}