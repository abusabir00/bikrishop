<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AffiliationHistory
 * @package App
 */
class AffiliationHistory extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'user_id',
        'affiliated_user_id',
        'amount',
        'status'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function affliatedBy()
    {
        return $this->belongsTo(User::class, 'affiliated_user_id');
    }
}
