<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Orderlist extends Model
{
  
    protected $table = 'order_list';

    protected $guarded=[];

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [];

}