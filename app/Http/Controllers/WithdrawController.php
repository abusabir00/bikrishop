<?php

namespace App\Http\Controllers;

use App\AffiliationAmount;
use App\AffiliationWithdraw;
use Illuminate\Http\Request;

class WithdrawController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $withdraws = AffiliationWithdraw::orderBy('id', 'desc')
            ->get();
        return view('admin.withdraw.list', compact('withdraws'));
    }

    public function action(Request $request, $id)
    {
        $user = AffiliationWithdraw::where('id', $id)
            ->first();
        if ($request->status == 'complete') {
            AffiliationAmount::where('user_id', $user->user_id)
                ->decrement('amount', $user->amount);
            $user->status = 'complete';
            $user->save();
            return redirect()
                ->back()
                ->with('success', 'Completion success');
        } else if ($request->status == 'rejected') {
            $user->status = 'rejected';
            $user->save();
            return redirect()
                ->back()
                ->with('success', 'Rejection success');
        }

        return redirect()
            ->back();
    }
}
