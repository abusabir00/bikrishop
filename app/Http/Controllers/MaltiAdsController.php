<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Maltiads;

class MaltiAdsController extends Controller
{
	public function adsFrom()
	{
		return view('maltiAds.adsFrom');
	}

	public function storeMaltiAds(Request $request)
	{
		$this->validate($request, [

			'campaignname' => 'required',
			'link' => 'required',
			'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
			'startdate' => 'required',
			'package' => 'required',
			'transectionId' => 'required',
			'mobileNum' => 'required'

		]);


		$multiAds  =new  Maltiads();
		$multiAds->campaignname = $request->campaignname;
		$multiAds->link = $request->link;
		$multiAds->startdate = $request->startdate;
		$multiAds->package = $request->package;
		$multiAds->mobile = $request->mobileNum;

		$path = "public/upload/ads/";
		$_fileAdsImage = $request->file('image');
		if ($request->hasFile('image')) {
			$adsImage = trim(sprintf("%s", uniqid(date('i') . 'ads_', true)) . $_fileAdsImage->getClientOriginalName());
			$mime_type = $_fileAdsImage->getClientMimeType();

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
				if (!file_exists($path)) {
					mkdir($path, 0777, true);
				}
				$_fileAdsImage->move($path, $adsImage);
			}
			$multiAds->image = $adsImage;		}

		$multiAds->transectionId = $request->transectionId;
		$multiAds->save();

		return redirect('/');
	}

}
