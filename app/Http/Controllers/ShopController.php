<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Shop;


class ShopController extends Controller{
     
     public function index(){
        
        $shop_cats=DB::table('shop_categories')->orderBy('id','asc')->get();
        $shop = Shop::get();

     	return view('shop.shop', compact('shop','shop_cats'));
     }
     
     public function order_last(){

      return view('shop.order-final', compact('shop'));
     }

     public function single($id){

     	$shop = Shop::where('id', $id)->first();

     	return view('shop.product-view', compact('shop'));
     }


     public function shop_cats(){

       

        

        $shop_cats=DB::table('shop_categories')->orderBy('id','asc')->get();
        $shop = Shop::get();

      return view('shop.shop', compact('shop','shop_cats'));

      // return view('shop.shop_cats', compact('shop'));

     }

     public function shop_cat_prods($cat_id){

       $shop_cat=DB::table('shop_categories')->where('id',$cat_id)->get();
       $shop_cat_name="";
       if(count($shop_cat)){

          $shop_cat_name=$shop_cat[0]->category_name;
       }
       

        $shop = Shop::where(['category_id'=>$cat_id])->get();

      return view('shop.shop_cat_prods', compact('shop','shop_cat_name'));

     }

     public function orderStore(Request $req){

          
           $productname = $req->input('productname');
           $fullname = $req->input('fullname');
           $mobilenumber = $req->input('mobilenumber');
           $fulladdress = $req->input('fulladdress');
           $tranxid = $req->input('tranxid');
           $purchasedate = $req->input('purchasedate');
           $order_id = $req->input('order_id');
           $email = $req->input('email');

           
           $data['productname'] = $productname;
           $data['fullname'] = $fullname;
           $data['mobilenumber'] = $mobilenumber;
           $data['fulladdress'] = $fulladdress;
           $data['tranxid'] = $tranxid;
           $data['purchasedate'] = $purchasedate;
           $data['order_id'] = $order_id;
           $data['email'] = $email;


           DB::table('order_list')->insert($data);

           return redirect()->back();

     }


     public function shopCatCreate(Request $request){

           return view('shop.create_cat');

     }//end of function shopCatCreate


     public  function shopCatCreateNow(Request $request){

         $cat_name = $request->category_name;

         echo " cat name = ".$cat_name;
         //exit;

         $this->validate($request, [
            'category_name' => 'required|unique:shop_categories'
            
        ]);

         DB::table('shop_categories')->insert(
           ['category_name' => $cat_name]
          );

        
        return redirect('shop_cats');





     }//end of function shopCatCreateNow


     public function shopCats(Request $request){

      $shop_cats=DB::table('shop_categories')->orderBy('id','desc')->get();

      return view('shop.shop_cats', compact('shop_cats'));

     }//end of function shopCats

}



?>