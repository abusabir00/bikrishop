<?php
namespace App\Http\Controllers\Admin;

use App\Package;
use App\User;
use App\Ads;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use DB;

/**
 * 
 */

class AdminpackageController extends Controller

{
     
     public function index(){

        $package = Package::leftJoin('users', 'users.id', '=', 'packages.user_id')->get(['packages.*', 'users.name']);

        

     	// $package = Package::get();

     	return view('admin.package.package', compact('package'));
     }


     public  function  packageListAll(){

        $packages_all=DB::table('package_create')->where(['deleted'=>0])->get();
        return view('admin.package.packages_all', compact('packages_all'));


     }//end of function packageListAll

     public  function boostPackageListAll(){

       $packages_all=DB::table('boost_packages')->where(['deleted'=>0])->get();
        return view('admin.package.boost_packages_all', compact('packages_all'));

     }// end of function boostPackageListAll

      public function updateStatus(Request $request)
	  {
	  	$user_id = $request->user_id;
	  	$status_id = $request->status_id;
      $package_id= $request->package_id;

	  	DB::table('packages')->where(['id'=>$package_id])->update(['status' => $status_id]);

	  	return 1;
	  }

    public function delete_usual_package_now(Request $request){

      $package_id = $request->package_id;

      
      DB::table('package_create')->where(['id'=>$package_id])
      ->delete();
      //->update(['deleted'=>1]);

      echo 'success';





    }// end of function delete_usual_package_now


    public function delete_boost_package_now(Request $request){

      $package_id = $request->package_id;
      
      DB::table('boost_packages')->where(['id'=>$package_id])
      ->delete();
      //->update(['deleted'=>1]);

      echo 'success';


    }//end of function delete_boost_package_now


    public function create(){

      return view('admin.package.create');
    }

    public function boostPackageCreate(){

      return view('admin.package.boost_package_create');

    }//end of function boostPackageCreate

     public function insert(Request $req)
    {

        $this->validate($req, [
            'packagename' => 'required|min:2|unique:package_create',
            'packageprice' => 'required|numeric',
            'packageqty' => 'required|numeric'
        ]);

        $packagename = $req->input('packagename');
        $packageprice = $req->input('packageprice');
        $packageqty = $req->input('packageqty');

        $data = [];

        $data['packagename'] = $packagename;
        $data['packageprice'] = $packageprice;
        $data['packageqty'] = $packageqty;


        DB::table('package_create')->insert($data);

        //return view('admin.package.create');

        return redirect('package-list-all');


    }

    public function  insert_boost_package(Request $req){


       $this->validate($req, [
            'boost_type' => 'required',
            'package_name' => 'required|min:2|unique:boost_packages',
            'package_price' => 'required|numeric|min:1',
            'package_day' => 'required|integer|min:1'
        ]);

        $boost_type = $req->input('boost_type');
        $packagename = $req->input('package_name');
        $packageprice = $req->input('package_price');
        $packageqty = $req->input('package_day');

        $data = [];

        $data['package_name'] = $packagename;
        $data['package_price'] = $packageprice;
        $data['package_day'] = $packageqty;
        $data['boost_type'] = $boost_type;


        DB::table('boost_packages')->insert($data);

        //return view('admin.package.create');

        return redirect('boost-package-list-all');

    }//end of function insert_boost_package




  }



?>