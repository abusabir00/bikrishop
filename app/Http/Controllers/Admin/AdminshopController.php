<?php

namespace App\Http\Controllers\Admin;

use App\Mail\OrderMail;
use App\Orderlist;
use App\Shop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

use DB;
use Illuminate\Support\Facades\Mail;

/**
 *
 */
class AdminshopController extends Controller

{

    public function index()
    {
        $shop_cats=DB::table('shop_categories')->orderBy('id','desc')->get();
        return view('admin.shop.product-add',compact('shop_cats'));
    }

    public function insert(Request $req)
    {
        $productname = $req->input('productname');
        $productprice = $req->input('productprice');
        $productdetails = $req->input('productdetails');
        $category_id = $req->input('category_id');

        $data = [];

        $path = "public/upload/shop/";
        // $_fileAdsImage = $req->file('image');
        $_fileAdsImages = $req->file('image');


        // var_dump($_fileAdsImages);
        if ($req->hasFile('image')) {

            

            $counter=0;
            foreach ($_fileAdsImages as $_fileAdsImage) {

                // if($_fileAdsImage){

                
                # code...
           // echo " original name = ".$_fileAdsImage->getClientOriginalName();
            $adsImage = trim(sprintf("%s", uniqid(date('i') . 'ads_', true)) . $_fileAdsImage->getClientOriginalName());
            $mime_type = $_fileAdsImage->getClientMimeType();

            if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
                if (!file_exists($path)) {
                    mkdir($path, 0777, true);
                }
                $_fileAdsImage->move($path, $adsImage);
            }

            ++$counter;

            $data['image'.$counter] = $adsImage;

           // }//end of  if($_fileAdsImage){

            }//end of foreach loop

        }//end of if 


        $data['productname'] = $productname;
        $data['productprice'] = $productprice;
        $data['productdetails'] = $productdetails;
        $data['category_id']=$category_id;


        DB::table('shop')->insert($data);

       $shop_cats=DB::table('shop_categories')->orderBy('id','desc')->get();


        return view('admin.shop.product-add', compact('shop_cats'));


    }


    public function list()
    {

        $order_list = Orderlist::get();

        return view('admin.shop.order-list', compact('order_list'));
    }

    public function productlist()
    {

        $product_list = Shop::get();

        return view('admin.shop.product-list', compact('product_list'));
    }


    public function delete_product($id)
    {

        $delete = Shop::where('id', $id)->delete();

        return redirect()->back();

    }


    // public function order_action($id){

    //     return view('admin.shop.order-check');

    // }

    public function order_action($id)
    {

        $order_action = Orderlist::find($id);

        return view('admin.shop.order-check', compact('order_action', 'id'));
    }

    public function update(Request $request)
    {
        $this->validate($request, [

            'status' => 'required',

        ]);
        $order_list = Orderlist::where('id', $request->id)
            ->first();
        $order_list->status= $request->status;
        $order_list->save();

        Mail::to($order_list->email)
            ->send(new OrderMail($request->status));

        return redirect()->back()->with('success', 'Order status changed');


    }


    public function product_action($id)
    {

        $product_action = Shop::find($id);

        return view('admin.shop.product-edit', compact('product_action', 'id'));
    }


    public function update_product(Request $request)
    {

        $this->validate($request, [

            'productname' => 'required',
            'productprice' => 'required',
            'productdetails' => 'required',
            'productstatus' => 'required',

        ]);

        DB::table('shop')
            ->where('id', $request->id)
            ->update([
                'productname' => $request->productname,
                'productprice' => $request->productprice,
                'productdetails' => $request->productdetails,
                'productstatus' => $request->productstatus,

            ]);

        return redirect()->back();


    }


}


?>