<?php

namespace App\Http\Controllers\Admin;

use App\City;
use App\Message;
use App\SaveAdd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\User;
use App\Ads;
use Carbon\Carbon;
use App\Region;
use App\thana;
use App\AdsImages;
use Yajra\Datatables\Datatables;



use App\FeaturedAds;
use App\PaymentGateway;
use App\Paypal;

use App\Maltiads;

use Crypt;
use Session;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads_view = $this->stats('ads_view');
        $profile_view = $this->stats('profile_view');
        $register_stats = $this->stats('register');
        $message_stats = $this->messageStats();


        $total_ads = Ads::count();
        $today_ads = Ads::whereRaw('date(created_at) = curdate()')->count();
        $today_user = User::where('type', '!=', 'adm')->whereRaw('date(created_at) = curdate()')->count();
        $total_user = User::where('type', '!=', 'adm')->count();
        $total_messages = Message::count();
        $today_messages = Message::whereRaw('date(created_at) = curdate()')->count();

        $total_save_ads = SaveAdd::count();
        $today_save_ads = SaveAdd::whereRaw('date(created_at) = curdate()')->count();

        return view("admin.dashboard", compact('message_stats','total_save_ads','today_save_ads', 'today_messages','total_messages','total_ads', 'total_user', 'today_ads', 'today_user','ads_view', 'profile_view', 'register_stats'));
    }

    public function report(Request $request){

        $report_result= DB::table('reports')
        ->join('ads','reports.ad_id','=','ads.id')
        /*->join('ads_images', function($join){

              $join->on('save_add.ad_id','=','ads_images.ad_id')

              ->where('save_add.ad_id', '=', DB::raw("(SELECT min(id) from ads_images WHERE save_add.ad_id = ads_images.ad_id)") )
              ->limit(1);

        

        })*/

        // ->where('save_add.ad_id', '=', DB::raw("(SELECT min(id) from ads_images WHERE save_add.ad_id = ads_images.ad_id "))
        //->join('ads_images','save_add.ad_id','=','ads_images.ad_id')
        

        // ->on('projectnotes.id', '=', DB::raw("(SELECT max(id) from projectnotes WHERE projectnotes.project_id = projects.id)")); 
        //})
        //})
        
        ->groupBy('reports.ad_id')
        // ->select('ads.id','ads_images.image','save_add.user_id','ads.title',
              // DB::raw('count(save_add.ad_id) as total'))
        ->select('reports.ad_id as ad_id_found','ads.id','reports.user_id','ads.title',
              DB::raw('count(reports.ad_id) as total'))
        
        
        ->get();

        return view('admin.ads.report',compact('report_result'));



    }// end of function report 

/*stats*/
    private function stats($type)
    {
        $date = new Carbon;
        $date->subMonth(12);

        $data=array();

        for($i=1; $i<13; $i++){            
            $add_month = $date->addMonth(1);
            $month = date('m', strtotime($add_month));
            $year = date('Y', strtotime($add_month));

            if ($type == 'ads_view' || $type == 'profile_view'){                
                $table = 'profile_visit';
            }else{                
                $table = 'users';
            }
            $stats = DB::table($table);
            $stats = $stats->select(DB::raw('count(id) as `total`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"), DB::raw('YEAR(created_at) year, MONTH(created_at) month'));
            $stats = $stats->whereYear('created_at', $year);
            $stats = $stats->whereMonth('created_at', $month);
            if ($type == 'ads_view' || $type == 'profile_view'){
                $stats = $stats->where([$type => 1]);                
            }else{
                $stats = $stats->where('type', '!=','adm');                
            }            
            $stats = $stats->groupby('year', 'month');
            $stats = $stats->get();
            if(count($stats)>0){
                $data[] = $stats[0]->total;
            }else{
                $data[] = 0;
            }
        }

        $data = implode(',', $data);

        if (strlen($data) > 0)
        {
            $data = $data;
        }else{
            $data = '0,0,0,0,0,0,0,0,0,0,0,0';
        }
        return $data;
    }

    /* message stats */
    private function messageStats()
    {
        $date = new Carbon;
        $date->subMonth(12);

        $data=array();

        for($i=1; $i<13; $i++){
            $add_month = $date->addMonth(1);
            $month = date('m', strtotime($add_month));
            $year = date('Y', strtotime($add_month));

            $stats = DB::table('message');
            $stats = $stats->select(DB::raw('count(id) as `total`'), DB::raw("DATE_FORMAT(created_at, '%m-%Y') new_date"), DB::raw('YEAR(created_at) year, MONTH(created_at) month'));
            $stats = $stats->whereYear('created_at', $year);
            $stats = $stats->whereMonth('created_at', $month);

            $stats = $stats->groupby('year', 'month');
            $stats = $stats->get();
            if(count($stats)>0){
                $data[] = $stats[0]->total;
            }else{
                $data[] = 0;
            }
        }

        $data = implode(',', $data);

        if (strlen($data) > 0)
        {
            $data = $data;
        }else{
            $data = '0,0,0,0,0,0,0,0,0,0,0,0';
        }
        return $data;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /*
     * Add Location
     * region
     * */

    function regionCreate(Request $request)
    {
        return view('admin.location.region');
    }
    // region
    function storeRegion(Request $request)
    {
        $obj = new Region();
        if ($request->id)
        {
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());

        if ($obj->save())
        {
            echo 1;
        }
    }
    function editRegion($id)
    {
        $region = Region::where('id', $id)->first();
        return view('admin.location.region', compact('region'));
    }

/*
 * City
 * */

    // create city
    function cityCreate(Request $request)
    {
        $region = Region::all()->all();
        return view('admin.location.city', compact('region'));
    }

    // city store
    function storeCity(Request $request)
    {
        $obj = new City();
        if ($request->id)
        {
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());

        if ($obj->save())
        {
            echo 1;
        }
    }
    // edit city
    function editCity($id)
    {
        $city = City::where('id', $id)->first();
        $region = Region::all();

        return view('admin.location.city', compact('city', 'region'));
    }

    public function thanaCreate()
    {
        $region = Region::get();
        return view('admin.location.thana',compact('region', 'thana'));
    }

    public function getCity(Request $request)
    {
        $region_id = $request->division_id;
        $city = DB::table('city')->where('region_id', $region_id)->get(['id', 'title']);
        
        $data = ['responseCode' => 1, 'data' => $city];
        return response()->json($data);
    }

    public function thanaStore(Request $request)
    {
        $this->validate($request, [
            'division_id' => 'required',
            'district_id' => 'required',
            'title' => 'required'
        ]);
        $check = thana::where('division_id', $request->division_id)->where('district_id', $request->district_id)->where('title', $request->title)->first();

        if (!empty($check)) {
              return 2;
          }  

       $obj = new thana();
        if ($request->id)
        {
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());

        if ($obj->save())
        {
            echo 1;
        }
    }

    public function loadThana(Request $request)
    {
        $data = DB::table('thanas')
            ->join('region', 'region.id', 'thanas.division_id')
            ->join('city', 'city.id', 'thanas.district_id')
            ->select('thanas.id', 'thanas.title as thana', 'region.title as division', 'city.title as district', 'thanas.created_at')->get();
        
        $count=0;
        return Datatables::of($data)
            ->addColumn('action', function ($data)
            {
                $b = '<a href="'.url('edit-thana/'.$data->id).'" title="Edit" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i></a> ';
                $b .= '<a onclick="deleteRow(this)" data-id="' . $data->id . '" data-obj="thana" href="javascript:;" title="Delete" class="btn btn-xs btn-danger danger-alert"><i class="glyphicon glyphicon-trash"></i></a> ';
                return $b;
            })
            ->editColumn('id', function($data){
                global $count;
                $count++;
                return $count;
            })
            ->make(true);
    }

    public function editThana($id)
    {
       $region = Region::get();
       $thana = thana::where('id', $id)->first();
        return view('admin.location.thana',compact('region', 'thana')); 
    }


    public function boostRequestShow(Request $request){


       
        return view('admin.ads.boost_request'); 

    }//end  of function boostRequestShow


    public function boostAdNow(Request $request){

        $ad_id = $request->id;

        $ad_found=Ads::find($ad_id);
        $boost_type=$ad_found->boost_type;

        Ads::where('id',$ad_id)->update(['boost_accept'=>1,
            'boost_type_accepted'=>$boost_type]);
        return 'success';


    }//end of function boostAdNow

    public  function boostDeleteNow(Request $request){


        $ad_id = $request->id;

        $ad_found=Ads::find($ad_id);
        $boost_type=$ad_found->boost_type;

        if($ad_found->boost_accept  ==0 ){

            Ads::where('id',$ad_id)->update(['boost_request'=>0,'boost_accept'=>0,
              'boost_type'=>$boost_type,'package_id'=>0,'transaction_id'=>'']);
        }

        
        return 'success';

    }// end of function boostdeleteNow 

    public  function loadBoostRequestLists(Request $request){

      // $data = Maltiads::get();
        $data = Ads::where(['boost_request'=>1, 'boost_accept'=>0])->get();

        return Datatables::of($data)

            ->editColumn('image', function($data){

                $ad_id=$data->id;
                $ads_image=AdsImages::where('ad_id', $ad_id)->select('image')->first();


               //if (!empty($data->image)) {

                
                  /*return '<img src="public/upload/ads/'.$ads_image->image.'" height="80px" width="180px">';*/

                  return '<img src="assets/images/listings/'.$ads_image->image.'" height="80px" width="180px">';

               /*}else{
                 return '';
               }*/
            })
            ->editColumn('user_id', function($data){
                $user_id=$data->user_id;
                $user=User::find($user_id);
                return $user->name;
            })
            ->editColumn('package_id', function($data){

                $urgent_package_id=$data->urgent_package_id;
                $urgent_package=DB::table('boost_packages')->where('id',$urgent_package_id)->first();
                if (isset($urgent_package)) {
                    $urgent_package=$urgent_package->package_name;
                }else{
                   $urgent_package='N/A'; 
                }

                $top_package_id=$data->top_package_id;
                $top_package=DB::table('boost_packages')->where('id',$top_package_id)->first();
                if (isset($top_package)) {
                    $top_package=$top_package_id->package_name;
                }else{
                   $top_package='N/A'; 
                }

                $bump_package_id=$data->bump_package_id;
                $bump_package=DB::table('boost_packages')->where('id',$bump_package_id)->first();
                if (isset($bump_package)) {
                    $bump_package=$bump_package->package_name;
                }else{
                   $bump_package='N/A'; 
                }
                

                return $package_name =  $urgent_package.', '.$top_package.', '.$bump_package;
                
                
            })
            ->editColumn('boost_type', function($data){
                
                $boost_type= $data->boost_type;
                $boost_type_text="";
                if($boost_type==trim("topad")){
                    $boost_type_text="Top Ad";

                }elseif ($boost_type==trim("urgent")) {

                    $boost_type_text="Urgent";

                }elseif($boost_type==trim("dailybump")){
                    $boost_type_text=" Daily bump";

                }

                return $boost_type_text;
            })
          
          /*  ->editColumn('startdate', function($data){
                return date('Y-m-d', strtotime($data->startdate));
            })*/
            ->addColumn('action', function ($data)
            {
                 $html = '';
                    $html = ' <a title="Boost" onclick="boostAdNow(this)" data-id="'.$data->id.'"  data-obj="user_ads" href="javascript:;"  class="btn btn-sm btn-info"> <i class="fa fa-plus"></i></a> ';

                 $html .= '<br/><br/><a title="Boost Delete" onclick="boostDeleteNow(this)" data-id="'.$data->id.'"  data-obj="user_ads" href="javascript:;"  class="btn btn-sm btn-danger"> <i class="fa fa-minus"></i></a> ';
                    
                return $html;
            })
            ->rawColumns([ 'action', 'image'])
            ->make(true);

    }//end of function loadBoostRequestLists

}
