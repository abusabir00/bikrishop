<?php

namespace App\Http\Controllers\Admin;

	
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Blog;
use DB;

/**
 * 
 */
class AdminblogController extends Controller
{
	
	public function index(){

		return view('admin.blogs.blog-post');
	}


	public function insert(Request $req)
	{
           
           $name = $req->input('name');
           $title = $req->input('title');
           $date = $req->input('date');
           $blogpost = $req->input('blogpost');

           $data = [];
             
           $path = "public/upload/ads/";
		$_fileAdsImage = $req->file('image');
		if ($req->hasFile('image')) {
			$adsImage = trim(sprintf("%s", uniqid(date('i') . 'ads_', true)) . $_fileAdsImage->getClientOriginalName());
			$mime_type = $_fileAdsImage->getClientMimeType();

			if ($mime_type == 'image/jpeg' || $mime_type == 'image/jpg' || $mime_type == 'image/png') {
				if (!file_exists($path)) {
					mkdir($path, 0777, true);
				}
				$_fileAdsImage->move($path, $adsImage);
			}
				
            $data['image'] = $adsImage;		
	}
           
           $data['name'] = $name;
           $data['title'] = $title;
           $data['date'] = $date;
           $data['post'] = $blogpost;


           DB::table('blog')->insert($data);

           return view('admin.blogs.blog-post');
           
	}

	public function blog_list(){
         
        $blog_list = Blog::get();

		return view('admin.blogs.blog-list', compact('blog_list'));
	}

	public function edit($id){

		$edit_blog = Blog::find($id);

		return view('admin.blogs.edit-blog', compact('edit_blog', 'id'));
	}

	public function update(Request $request){
        
         $this->validate($request,[

              'name' => 'required',
              'title' => 'required',
              'post' => 'required',
              'date' => 'required',
         ]);

		 DB::table('blog')
		 ->where('id', $request->id)
		 ->update([
           'name' => $request->name,  
           'title' => $request->title,  
           'post' => $request->post,  
           'date' => $request->date,  
		 ]);

         return redirect()->back();


	}


	public function delete($id){

         $delete = Blog::where('id', $id)->delete();

         return redirect()->back();
	}
}