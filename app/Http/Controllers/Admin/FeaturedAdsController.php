<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\FeaturedAds;
use App\PaymentGateway;
use App\Paypal;
use Illuminate\Http\Request;
use App\Maltiads;
use Yajra\Datatables\Datatables;
use Crypt;
use Session;
use DB;

class FeaturedAdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $featured_ads = FeaturedAds::first();
        $paymentGateway = PaymentGateway::first();

        return view('admin.featured_ads.index', compact('featured_ads', 'paymentGateway'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $obj = new FeaturedAds();
        if($request->id!=''){
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());
        $status = 0;
        if($request->status){
            $status = 1;
        }
        $obj->status = $status;
        if($obj->save()){
           return response()->json(['msg' => 1, 'id' => $obj->id]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function paymentGatewaySave(Request $request){
        $obj = new PaymentGateway();
        if($request->id!=''){
            $obj = $obj->findOrFail($request->id);
        }
        $obj->fill($request->all());
        $stripe_status = 0;
        if($request->stripe_status){
            $stripe_status = 1;
        }
        $obj->stripe_status = $stripe_status;

        /* paypal status */
        $paypal_status = 0;
        if($request->paypal_status){
            $paypal_status = 1;
        }
        $obj->paypal_status = $paypal_status;

        if($obj->save()){
            return response()->json(['msg' => 1, 'id' => $obj->id]);
        }
    }

    public function multiAdsRequest()
    {
        return view('admin.ads.multi_ads_request');
    }

    public function loadAdsRequestLists()
    {
        $data = Maltiads::get();
        return Datatables::of($data)
            ->editColumn('image', function($data){
               if (!empty($data->image)) {
                  return '<img src="public/upload/ads/'.$data->image.'" height="80px" width="180px">';
               }else{
                 return '';
               }
            })
            ->editColumn('status', function($data){
                $current_date = Date("Y-m-d H:i:s");

                if ($current_date <= $data->enddate) {
                    if ($data->status == 0){
                    return "<label class='btn btn-info btn-xs'>Pending</label>";
                }else if($data->status == 1){
                    return "<label class='btn btn-success btn-xs'>Active</label>";
                }else{
                    return " ";
                }

                }else{
                 return "<label class='btn btn-info btn-xs'>Expared</label>";
                }
            })
            ->editColumn('startdate', function($data){
                return date('Y-m-d', strtotime($data->startdate));
            })
            ->addColumn('action', function ($data)
            {
                 $html = '';
                    $html = ' <a href="' . url('multi-ads/edit/'. Crypt::encrypt($data->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i></a> ';
                    $html .= ' <a href="' . url('multi-ads/view/'. Crypt::encrypt($data->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-eye"></i></a> ';
                    $html .= ' <a href="' . url('multi-ads/delete/' . Crypt::encrypt($data->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i></a> ';
                return $html;
            })
            ->rawColumns(['status', 'action', 'image'])
            ->make(true);
    }

    public function MultiadsEditView($id)
    {
        $id = Crypt::decrypt($id);
        $multiAds = Maltiads::find($id);

        return view('admin.ads.edit_multi_ads',compact('multiAds'));
    }

    public function MultiadsUpdate(Request $request)
    {
        $this->validate($request, [
            'status' => 'required',
            'enddate' => 'required'
        ]);

        Maltiads::where('id', $request->id)
       ->update([
           'status' => $request->status,
           'enddate' => $request->enddate
        ]);

       Session::flash('success', 'Data is updated successfully!');

       return redirect()->back();
    }

    public function MultiadsDelete($id)
    {
     $id = Crypt::decrypt($id);
     DB::table('maltiads')->where('id', $id)->delete();
     Session::flash('success', 'Data is deleted successfully!');
     return redirect()->back();   
    }

    public function SingleView($id)
    {
        $id = Crypt::decrypt($id);
        $multiAds = Maltiads::find($id);
        return view('admin.ads.view-multiads', compact('multiAds'));
    }
}
