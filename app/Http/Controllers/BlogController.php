<?php

namespace App\Http\Controllers;

use App\Blog;


class BlogController extends Controller
{
	
	public function index(){
        
        $blog = Blog::get();
       
        $sideblog = Blog::orderBy('id', 'desc')->take(8)->get();

		return view('blogs.blog', compact('blog','sideblog'));

		
	}

	public function single($id){
  
         $blog = Blog::where('id', $id)->first();
   
         return view('blogs.single-blog', compact('blog'));
	}
}


?>