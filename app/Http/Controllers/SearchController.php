<?php

namespace App\Http\Controllers;

use App\City;
use App\thana;
use Illuminate\Http\Request;
use DB;
use App\Ads;
use App\Category;
use App\Region;
use App\CategoryCustomfields;
use Auth;

class SearchController extends Controller
{

    private $where, $category, $region, $city, $thana, $price_range, $price_sort, $keyword = '';
    private $custom_search = false;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        //echo 'asdf';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function searchAllAds(Request $request){

        /*******/
           // $user_id = Auth::user()->id;
           


           $boost_data = DB::table('ads')
            ->join('boost_packages', 'boost_packages.id', 'ads.package_id')
            
            ->select('ads.id', 'ads.updated_at', 'boost_packages.package_day')->get();

            foreach ($boost_data as $boost_indiv) {
                
                $updated_at=$boost_indiv->updated_at;
                $updated_at_array=explode(' ', $updated_at);
                $updated_at_date=$updated_at_array[0];
                $today=date('Y-m-d');

                // echo " updated at = ".$updated_at_date." today = ".$today;

                $package_day = $boost_indiv->package_day;

                // echo " difference = ". (strtotime($today) - strtotime($updated_at_date)." boost id = ".$boost_indiv->id." package d ay = ".$package_day);

                if((strtotime($today) - strtotime($updated_at_date) )>$package_day ){

                    $boost_id=$boost_indiv->id;

                    $update_sql=Ads::where('id',$boost_id)->update(['boost_request'=>0, 'boost_accept'=>0, 'boost_type'=>'', 
                        'boost_type_accepted'=>'', 'package_id'=>0, 
                        'transaction_id'=>'']);

                    // echo " update sql = ".$update_sql;



                }




            }




        /*******/

        // print_r($request->all());
        // exit;
        $result = array();
        $category_id = '';
        $this->keyword = $request->keyword;
        $this->price_sort = $request->price_sort;
        $this->price_range = $request->price_range;

        if (isset($request->reg)) {
            $this->region = Region::where('title', str_replace('-', ' ', $request->reg))->value('id');
        } else if (isset($request->region)) {
            $this->region = $request->region;
        }

        $this->city = $request->city;
        $this->thana = $request->thana;

        if ($request->category != '' && is_numeric($request->category)) {
            $category_id = $request->category;
        } else if ($request->main_category != '') {
            //echo 'ok';
            $category_id = Category::where('slug', urldecode($request->main_category))->value('id');

            //echo " category_id ==== ".$category_id;
        }

        $cat = Category::parent($category_id)->renderAsArray();
        //echo " cat = ";
        //var_dump($cat);
        $child_ids = Category::childIds($cat);
        // echo " child ids = ";
        // var_dump($child_ids);
        // array_push($child_ids, $category_id);

        $this->category = $child_ids;

        // custom search
        $totalCf = 0;
        $cf_req_array = array();
        if (is_array($request->custom_search)) {

            
            $where = '';
            foreach ($request->custom_search as $index => $item) {
                if ($index != '' && $item != '') {
                    $this->custom_search = true;
                    $where .= 'custom_field_data.column_name = "' . $index . '" and custom_field_data.column_value = "' . $item . '" OR ';
                    $totalCf++;
                    array_push($cf_req_array, $item);
                }
            }
            $this->where = rtrim(ltrim($where), 'OR ');
        }else

//exit;
        $sql_search = Ads::with(array('region' => function ($query) {
                if ($this->region != '') {
                    $query->where('region.id', $this->region);
                }
            }, 'city' => function ($query) {
                $query->where('city.id', $this->city);
            }, 'thana' => function ($query) {
                $query->where('thanas.id', $this->thana);
            },

                'category' => function ($query) {
                    $query->whereIn('categories.id', $this->category);
                }
            , 'save_add' => function ($query) {
                    if (!Auth::guest()) {
                        $query->where('save_add.user_id', Auth::user()->id);
                    }
                }
            , 'ad_cf_data' => function ($query) {

                    $query->join('customfields', 'customfields.id', '=', 'custom_field_data.cf_id');
                    $query->where('is_shown', 1);
                    if ($this->custom_search == true) {
                        $query->whereRaw($this->where);
                    }
                },
                'ad_images', 'city', 'user'
                //'boost_accept' added by me 
            )
        );

        //echo " sql search = ".$sql_search;


        if ($this->region != '') {
            $sql_search = $sql_search->where('region_id', $this->region);
        }

        if ($this->city != '') {
            $sql_search = $sql_search->where('city_id', $this->city);
        }

        if ($this->thana != '') {
            $sql_search = $sql_search->where('thana_id', $this->thana);
        }

        if ($this->category != '') {
            $sql_search = $sql_search->whereIn('category_id', $child_ids);
        }
        // keyword
        if ($request->keyword != '') {
            $sql_search = $sql_search->where('title', 'LIKE', '%'.$request->keyword . '%');
        }
        // is image
        if ($request->image != '') {
            $sql_search = $sql_search->where('is_img', $request->image);
        }
        // price sort
        if ($request->price_sort != '') {
            $sql_search = $sql_search->orderBy('price', $request->price_sort);
        }
        // price range
        if ($request->price_range != '') {
            $p_range = explode(';', $request->price_range);
            $sql_search = $sql_search->whereBetween('price', [$p_range[0], $p_range[1]]);
        }

        if ($request->online == 1 && $request->offline != 2) {
            $sql_search = $sql_search->where('is_login', 1);
        } elseif ($request->online == 2 && $request->offline != 1) {
            $sql_search = $sql_search->where('is_login', 0);
        }
        $sql_search = $sql_search->where('status', 1);
        $sql_search = $sql_search->orderByRaw("FIELD(f_type , 'top_page_price', 'urgent_top_price', 'urgent_price','home_page_price', '') ASC");


        /*$sql_search=$sql_search
        ->orderBy('boost_type_accepted','DESC')
        ->inRandomOrder(); //Added by me 
*/
        $top_ad_query=clone $sql_search;
        $bump_ad_query=clone $sql_search;
        $non_boost_ad_query=clone $sql_search;
        $urgent_ads = $sql_search->where('boost_type_accepted','urgent')->inRandomOrder()->limit(2)->get();
        

        $top_ads = $top_ad_query->where('boost_type_accepted','topad')->inRandomOrder()->limit(2)->get();

        $bump_ads = $bump_ad_query->where('boost_type_accepted','dailybump')->inRandomOrder()->limit(2)->get();


        $non_boost_ads = $non_boost_ad_query->where('boost_type_accepted','')
        ->orderBy('id','DESC')
        ->get();

        /* DB::enableQueryLog();
         dd($sql_search);*/
        
        // $boost_accept = $sql_search->boost_accept;

        $total = $sql_search->count();

        //DB::enableQueryLog(); // Enable query log



        //dd(DB::getQueryLog()); // Show results of log

        //$sql = $sql_search->toSql();

        // echo " sql = ".$sql;

        // exit;

       // $bindings = $query->getBindings();



        $sql_search = $sql_search
            ->paginate(10)
            ->appends(request()
                ->query());

            


        //      echo '<pre>';
        // print_r($sql_search);
        // echo '</pre>';

        // exit;
        error_reporting(0);
        if ($total > 0) {

            if (isset($sql_search) && count($sql_search[0]->category) > 0 && count($sql_search[0]->city) > 0 && count($sql_search[0]->region) > 0) {

                if ($this->custom_search == true) {
                    if (count($sql_search[0]->ad_cf_data) < 1) {
                        $result = array();
                    }

                } else {
                    $result = $sql_search;
                }
            }
        }

        $category = $request->category;
        //extra search
        $search_fields = DB::table('customfields')
            ->join('category_customfields', 'customfields.id', 'customfields_id')
            ->where(
                [
                    'category_customfields.category_id' => $category,
                    'customfields.search' => 1
                ]
            )
            ->select(
                'customfields.name',
                'customfields.options'
            )
            ->get()->toArray();

        // regions
        $regions = Region::all();
        // get search fields

        // category
        //$select_category = Category::attr(['name' => 'category', 'class' => 'form-control lselect', 'id' => 'category'])->renderAsDropdown();
        $cat = Category::all()->where('status', 1)->toArray();
        $category = array(
            'categories' => array(),
            'parent_cats' => array()
        );
        //build the array lists with data from the category table
        foreach ($cat as $row) {
            //creates entry into categories array with current category id ie. $categories['categories'][1]
            $category['categories'][$row['id']] = $row;
            //creates entry into parent_cats array. parent_cats array contains a list of all categories with children
            $category['parent_cats'][$row['parent_id']][] = $row['id'];
        }
        $req_category = $category_id;

        $city = DB::table('city')->get();
        $editMode = 'on';
        $multiAdsShow = DB::table('maltiads')->where('status', 1)->get();

        $cities = City::all();
        $thanas = thana::all();


        if ($request->has('region') and $request->get('region')){
            $regions = Region::where('id', $request->region)
                ->get();
            if ($request->has('city') and $request->get('city')){
                $cities = City::where('id', $request->city)
                    ->get();
                if ($request->has('thana') and $request->get('thana')){
                    $thanas = thana::where('id', $request->thana)
                        ->get();
                }elseif($request->has('thana') and $request->has('city') and $request->get('city')){
                    $thanas = thana::where('district_id', $request->city)
                        ->get();
                }
            }elseif ($request->has('city') and !$request->get('city') and $request->has('region') and $request->get('region')){
                $cities = City::where('region_id', $request->region)
                    ->get();
            }
        }

          //multi ads show 
        $multiAdsShow = DB::table('maltiads')->where('status', 1)
        ->orderByRaw('RAND()')// orderByRaw added by me 
        ->get();

        return view('search.show_all_ads', compact('search_fields', 'result', 'total','thanas', 'cities' ,'regions', 'category', 'req_category', 'city', 'editMode', 'multiAdsShow','multiAdsShow','urgent_ads','top_ads','bump_ads','non_boost_ads'));
    }//end of function searchAllAds

    public function search(Request $request)
    {

        /*******/
           //$user_id = Auth::user()->id;
           


           $boost_data = DB::table('ads')
            ->join('boost_packages', 'boost_packages.id', 'ads.package_id')
            
            ->select('ads.id', 'ads.updated_at', 'boost_packages.package_day')->get();

            foreach ($boost_data as $boost_indiv) {
                
                $updated_at=$boost_indiv->updated_at;
                $updated_at_array=explode(' ', $updated_at);
                $updated_at_date=$updated_at_array[0];
                $today=date('Y-m-d');

                // echo " updated at = ".$updated_at_date." today = ".$today;

                $package_day = $boost_indiv->package_day;

                // echo " difference = ". (strtotime($today) - strtotime($updated_at_date)." boost id = ".$boost_indiv->id." package d ay = ".$package_day);

                if((strtotime($today) - strtotime($updated_at_date) )>$package_day ){

                    $boost_id=$boost_indiv->id;

                    $update_sql=Ads::where('id',$boost_id)->update(['boost_request'=>0, 'boost_accept'=>0, 'boost_type'=>'', 
                        'boost_type_accepted'=>'', 'package_id'=>0, 
                        'transaction_id'=>'']);

                    // echo " update sql = ".$update_sql;



                }




            }




        /*******/

        // print_r($request->all());
        // exit;
        $result = array();
        $category_id = '';
        $this->keyword = $request->keyword;
        $this->price_sort = $request->price_sort;
        $this->price_range = $request->price_range;

        if (isset($request->reg)) {
            $this->region = Region::where('title', str_replace('-', ' ', $request->reg))->value('id');
        } else if (isset($request->region)) {
            $this->region = $request->region;
        }

        $this->city = $request->city;
        $this->thana = $request->thana;

        if ($request->category != '' && is_numeric($request->category)) {
            $category_id = $request->category;
        } else if ($request->main_category != '') {
            //echo 'ok';
            $category_id = Category::where('slug', urldecode($request->main_category))->value('id');

            //echo " category_id ==== ".$category_id;
        }

        $cat = Category::parent($category_id)->renderAsArray();
        //echo " cat = ";
        //var_dump($cat);
        $child_ids = Category::childIds($cat);
        // echo " child ids = ";
        // var_dump($child_ids);
        // array_push($child_ids, $category_id);

        $this->category = $child_ids;

        // custom search
        $totalCf = 0;
        $cf_req_array = array();
        if (is_array($request->custom_search)) {

            
            $where = '';
            foreach ($request->custom_search as $index => $item) {
                if ($index != '' && $item != '') {
                    $this->custom_search = true;
                    $where .= 'custom_field_data.column_name = "' . $index . '" and custom_field_data.column_value = "' . $item . '" OR ';
                    $totalCf++;
                    array_push($cf_req_array, $item);
                }
            }
            $this->where = rtrim(ltrim($where), 'OR ');
        }else

//exit;
        $sql_search = Ads::with(array('region' => function ($query) {
                if ($this->region != '') {
                    $query->where('region.id', $this->region);
                }
            }, 'city' => function ($query) {
                $query->where('city.id', $this->city);
            }, 'thana' => function ($query) {
                $query->where('thanas.id', $this->thana);
            },

                'category' => function ($query) {
                    $query->whereIn('categories.id', $this->category);
                }
            , 'save_add' => function ($query) {
                    if (!Auth::guest()) {
                        $query->where('save_add.user_id', Auth::user()->id);
                    }
                }
            , 'ad_cf_data' => function ($query) {

                    $query->join('customfields', 'customfields.id', '=', 'custom_field_data.cf_id');
                    $query->where('is_shown', 1);
                    if ($this->custom_search == true) {
                        $query->whereRaw($this->where);
                    }
                },
                'ad_images', 'city', 'user'
                //'boost_accept' added by me 
            )
        );

        //echo " sql search = ".$sql_search;


        if ($this->region != '') {
            $sql_search = $sql_search->where('region_id', $this->region);
        }

        if ($this->city != '') {
            $sql_search = $sql_search->where('city_id', $this->city);
        }

        if ($this->thana != '') {
            $sql_search = $sql_search->where('thana_id', $this->thana);
        }

        if ($this->category != '') {
            $sql_search = $sql_search->whereIn('category_id', $child_ids);
        }
        // keyword
        if ($request->keyword != '') {
            $sql_search = $sql_search->where('title', 'LIKE', '%'.$request->keyword . '%');
        }
        // is image
        if ($request->image != '') {
            $sql_search = $sql_search->where('is_img', $request->image);
        }
        // price sort
        if ($request->price_sort != '') {
            $sql_search = $sql_search->orderBy('price', $request->price_sort);
        }
        // price range
        if ($request->price_range != '') {
            $p_range = explode(';', $request->price_range);
            $sql_search = $sql_search->whereBetween('price', [$p_range[0], $p_range[1]]);
        }

        if ($request->online == 1 && $request->offline != 2) {
            $sql_search = $sql_search->where('is_login', 1);
        } elseif ($request->online == 2 && $request->offline != 1) {
            $sql_search = $sql_search->where('is_login', 0);
        }
        $sql_search = $sql_search->where('status', 1);
        $sql_search = $sql_search->orderByRaw("FIELD(f_type , 'top_page_price', 'urgent_top_price', 'urgent_price','home_page_price', '') ASC");


/*        $sql_search=$sql_search
        ->orderBy('boost_type_accepted','DESC')
        ->inRandomOrder(); //Added by me 
*/


        $top_ad_query=clone $sql_search;
        $bump_ad_query=clone $sql_search;
        $non_boost_ad_query=clone $sql_search;
        $urgent_ads = $sql_search->where('boost_type_accepted','urgent')->inRandomOrder()->limit(2)->get();
        

        $top_ads = $top_ad_query->where('boost_type_accepted','topad')->inRandomOrder()->limit(2)->get();

        $bump_ads = $bump_ad_query->where('boost_type_accepted','dailybump')->inRandomOrder()->limit(2)->get();


        $non_boost_ads = $non_boost_ad_query->where('boost_type_accepted','')->get();

        /* DB::enableQueryLog();
         dd($sql_search);*/
        
        // $boost_accept = $sql_search->boost_accept;

        $total = $sql_search->count();

        //DB::enableQueryLog(); // Enable query log



        //dd(DB::getQueryLog()); // Show results of log

        //$sql = $sql_search->toSql();

        // echo " sql = ".$sql;

        // exit;

       // $bindings = $query->getBindings();



        $sql_search = $sql_search
            ->paginate(10)
            ->appends(request()
                ->query());

            


        //      echo '<pre>';
        // print_r($sql_search);
        // echo '</pre>';

        // exit;
        error_reporting(0);
        if ($total > 0) {

            if (isset($sql_search) && count($sql_search[0]->category) > 0 && count($sql_search[0]->city) > 0 && count($sql_search[0]->region) > 0) {

                if ($this->custom_search == true) {
                    if (count($sql_search[0]->ad_cf_data) < 1) {
                        $result = array();
                    }

                } else {
                    $result = $sql_search;
                }
            }
        }

        $category = $request->category;
        //extra search
        $search_fields = DB::table('customfields')
            ->join('category_customfields', 'customfields.id', 'customfields_id')
            ->where(
                [
                    'category_customfields.category_id' => $category,
                    'customfields.search' => 1
                ]
            )
            ->select(
                'customfields.name',
                'customfields.options'
            )
            ->get()->toArray();

        // regions
        $regions = Region::all();
        // get search fields

        // category
        //$select_category = Category::attr(['name' => 'category', 'class' => 'form-control lselect', 'id' => 'category'])->renderAsDropdown();
        $cat = Category::all()->where('status', 1)->toArray();
        $category = array(
            'categories' => array(),
            'parent_cats' => array()
        );
        //build the array lists with data from the category table
        foreach ($cat as $row) {
            //creates entry into categories array with current category id ie. $categories['categories'][1]
            $category['categories'][$row['id']] = $row;
            //creates entry into parent_cats array. parent_cats array contains a list of all categories with children
            $category['parent_cats'][$row['parent_id']][] = $row['id'];
        }
        $req_category = $category_id;

        $city = DB::table('city')->get();
        $editMode = 'on';
        $multiAdsShow = DB::table('maltiads')->where('status', 1)->get();

        $cities = City::all();
        $thanas = thana::all();


        if ($request->has('region') and $request->get('region')){
            $regions = Region::where('id', $request->region)
                ->get();
            if ($request->has('city') and $request->get('city')){
                $cities = City::where('id', $request->city)
                    ->get();
                if ($request->has('thana') and $request->get('thana')){
                    $thanas = thana::where('id', $request->thana)
                        ->get();
                }elseif($request->has('thana') and $request->has('city') and $request->get('city')){
                    $thanas = thana::where('district_id', $request->city)
                        ->get();
                }
            }elseif ($request->has('city') and !$request->get('city') and $request->has('region') and $request->get('region')){
                $cities = City::where('region_id', $request->region)
                    ->get();
            }
        }

          //multi ads show 
        $multiAdsShow = DB::table('maltiads')->where('status', 1)
        ->orderByRaw('RAND()')// orderByRaw added by me 
        ->get();

        return view('search.index', compact('search_fields', 'result', 'total','thanas', 'cities' ,'regions', 'category', 'req_category', 'city', 'editMode', 'multiAdsShow','multiAdsShow','urgent_ads','top_ads','bump_ads','non_boost_ads'));
    }

    function ajaxSearch(Request $request)
    {
        $where = '';
        foreach ($request->all() as $index => $item) {
            $where .= 'column_name = "' . $index . '" and column_value = "' . $item . '" OR ';
        }
        echo $result = rtrim($where, 'OR ');
    }


}
