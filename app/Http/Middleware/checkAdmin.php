<?php
namespace App\Http\Middleware;
use Closure;

use Auth;
class checkAdmin
{
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::User()->type == 'adm' || Auth::User()->type == 'sa')
        {
        	return $next($request);
        }
        return response('Unauthorized.', 401);
        
    }
}
