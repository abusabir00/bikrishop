<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model

{

    protected $table = 'packages';

    protected $guarded=[];

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [];

}