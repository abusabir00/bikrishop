<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AffiliationWithdraw extends Model
{
    protected $fillable = [
        'user_id',
        'amount',
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
