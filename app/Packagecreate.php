<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packagecreate extends Model

{

    protected $table = 'package_create';

    protected $guarded=[];

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    protected $fillable = [];



    /**

     * The attributes that should be hidden for arrays.

     *

     * @var array

     */

    protected $hidden = [];

}